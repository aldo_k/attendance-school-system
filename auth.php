<?php
error_reporting(0);

require(__DIR__ . '/modules/config.php');

switch (@$_GET['p']) {
	// Page Login
	case 'login':
	require(__DIR__ . "/modules/_auth/login.php");
	break;
	// Page Logout
	case 'logout':
	require(__DIR__ . "/modules/_auth/logout.php");
	break;
	// Default Case Direct To index.php
	default:
	echo "<script>location.href='index.php';</script>";
	break;
}