<?php
// mendefinisikan semua halaman
$allPages = [
	'web' => [
		"admin", "dashboard", "guru", "jadwal", "jurusan",
		"kehadiran","kehadiran_siswa", "kelas", "password", "periode", "siswa","print_kehadiran"
	],
	'api' => [
		"admin", "dashboard", "guru", "jadwal", "jurusan",
		"kehadiran", "kelas", "password", "periode", "siswa",
	],
];

// mendefinisikan halaman apa saja yang dapat diakses oleh pengawas
$acl = [
	'web' => [
		// Admin
		'1'=>[ 
			"admin", "dashboard", "guru", "jadwal", "jurusan",
			"kehadiran", "kelas", "password", "periode", "siswa","print_kehadiran"
		],

		// Guest 1
		'2'=>[ 
			"admin", "dashboard", "guru", "jadwal", "jurusan","kehadiran_siswa",
			"kehadiran", "kelas", "password", "periode", "siswa","print_kehadiran",
		],

		// Guest 2
		'3'=>[ 
			"admin", "dashboard", "guru", "jadwal", "jurusan",
			"kehadiran", "kelas", "password", "periode", "siswa",
		],
	],

	'api' => [
		// Admin
		'1'=>[ 
			"admin", "dashboard", "guru", "jadwal", "jurusan",
			"kehadiran", "kelas", "password", "periode", "siswa",
		],

		// Guest 1
		'2'=>[ 
			"admin", "dashboard", "guru", "jadwal", "jurusan",
			"kehadiran", "kelas", "password", "periode", "siswa",
		],

		// Guest 2
		'3'=>[ 
			"admin", "dashboard", "guru", "jadwal", "jurusan",
			"kehadiran", "kelas", "password", "periode", "siswa",
		],
	]
];
