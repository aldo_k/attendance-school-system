<?php
require(__DIR__ . '/../vendor/autoload.php');

if(!isset($_SESSION)) { session_start(); } 

// Database Config For PDOx Library
$config = [
	'db' => [
		'host'		=> 'localhost',
		'driver'	=> 'mysql',
		'database'	=> 'ams_db',
		'username'	=> 'root',
		'password'	=> '',
		'charset'	=> 'utf8',
		'collation'	=> 'utf8_general_ci',
		'prefix'	 => '',
	]
];

// https://github.com/izniburak/pdox
$db = new \Buki\Pdox($config['db']);

// https://github.com/rakit/validation
$v 	= new \Rakit\Validation\Validator;

$v->setMessages([
	'required' => ':attribute harus diisi',
	'email' => ':email tidak valid',
	'same' => ':attribute tidak valid',
]);

// Access Control List (Listing Hak Akses)
require (__DIR__ . '/acl.php');
// Function (Kumpulan Fungsi Dasar)
require (__DIR__ . '/function.php');
