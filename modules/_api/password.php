<?php
$validation = $v->make($_POST, [
  'password'  => 'required',
  'newpassword'  => 'required|min:8',
  'cnewpassword'  => 'required|same:newpassword',
]);

// or this way:
$validation->setAliases([
  'password' => 'Password Lama',
  'newpassword' => 'Password Baru',
  'cnewpassword' => 'Konfirmasi Password',
]);

$validation->validate();

$Gagal 		= 0;
$Berhasil = 0;

if($isValidToken && !$validation->fails()):
  // Get Data Users Form Session Username Store
  $records = $db->table('users')
  ->select('*')
  ->where('username', $_SESSION['Username'])
  ->get();

  if($records->password==md5($_POST['password'])){

    $sql = $db->table('users')->where('username', $_SESSION['Username'])->update(['password'=>md5(@$_POST['newpassword'])]);

    $sql ?	$Berhasil=1 : $Gagal=1;
  } else {
    $Gagal=1;
  }
endif;

$message = [
  'csrf_token'=> [
    'status' => $isValidToken,
    'newToken'=> csrf_token(),
  ],
  'form' => $validation->errors()->all()
];

if ($Berhasil==1):
  echo json_encode([
    'error'=>false,
    'message'=> $message,
  ]);
else:
  echo json_encode([
    'error'=>true,
    'message'=> $message,
  ]);
endif;

