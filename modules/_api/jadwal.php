<?php
$status = true;

$table = 'schedule';
$primaryKey = 'schedule_id';
$joinQuery = "FROM schedule LEFT JOIN teacher USING(`teacher_id`) LEFT JOIN class USING(`class_id`)";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "*";

if ((int)@$_POST['period_id']>0) {
	$period_id = (int) $_POST['period_id'];
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`period_id` = '$period_id' ";
	} else {
		$extraWhere .= " AND `schedule`.`period_id` = '$period_id' ";
	}
} else if (@$_POST['period_id']=="all" && $_SESSION['Level']!=1) {
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`period_id` = '' ";
	} else {
		$extraWhere .= " AND `schedule`.`period_id` = '' ";
	}
}

if ((int)@$_POST['class_id']>0) {
	$class_id = (int) $_POST['class_id'];
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`class_id` = '$class_id' ";
	} else {
		$extraWhere .= " AND `schedule`.`class_id` = '$class_id' ";
	}
} else if (@$_POST['class_id']=="all" && $_SESSION['Level']!=1) {
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`class_id` = '' ";
	} else {
		$extraWhere .= " AND `schedule`.`class_id` = '' ";
	}
}

if ((int)@$_POST['teacher_id']>0) {
	$teacher_id = (int) $_POST['teacher_id'];
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`teacher_id` = '$teacher_id' ";
	} else {
		$extraWhere .= " AND `schedule`.`teacher_id` = '$teacher_id' ";
	}
} else if (@$_POST['teacher_id']=="all" && $_SESSION['Level']!=1) {
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`teacher_id` = '' ";
	} else {
		$extraWhere .= " AND `schedule`.`teacher_id` = '' ";
	}
}

if ((int)@$_POST['schedule_day']>0) {
	$schedule_day = (int) $_POST['schedule_day'];
	if ($extraWhere=="") {
		$extraWhere .= "`schedule`.`schedule_day` = '$schedule_day' ";
	} else {
		$extraWhere .= " AND `schedule`.`schedule_day` = '$schedule_day' ";
	}
}

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'schedule_id', 'dt' => 'schedule_id'],
	['db' => 'schedule_code', 'dt' => 'schedule_code'],
	['db' => 'schedule_name', 'dt' => 'schedule_name'],
	['db' => 'schedule_day', 'dt' => 'schedule_day'],
	['db' => 'schedule_starttime', 'dt' => 'schedule_starttime'],
	['db' => 'schedule_endtime', 'dt' => 'schedule_endtime'],
	['db' => 'class_id', 'dt' => 'class_id'],
	['db' => 'class_room', 'dt' => 'class_room'],
	['db' => 'teacher_name', 'dt' => 'teacher_name'],
	['db' => 'period_id', 'dt' => 'period_id'],
];

// List Validasi
$validation = $v->make($_POST, [
	'schedule_code'  => 'required|min:3',
	'schedule_name'  => 'required',
	'schedule_day'  => 'required',
	'schedule_starttime'  => 'required',
	'schedule_endtime'  => 'required',
	'class_id'  => 'required',
	'period_id'  => 'required',
	'teacher_id'  => 'required',
]);

// Aliase Form Name
$validation->setAliases([
	'schedule_code'  => 'Kode Jadwal Pelajaran',
	'schedule_name'  => 'Nama Mata Pelajaran',
	'schedule_day'  => 'Hari [Senin s/d Minggu]',
	'schedule_starttime'  => 'Jam Mulai',
	'schedule_endtime'  => 'Jam Selesai',
	'class_id'  => 'Kelas',
	'period_id'  => 'Periode',
	'teacher_id'  => 'Guru',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;

	// Cari Data
	case 'read':
	// Cetak Data Json
	echo json_encode($db->select($select)->table($table)->where($primaryKey,$id)->get());
	break;

	// Proses Create Data
	case 'create':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Tambah Data
		try{
			$db->table($table)->insert($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message]);
	break;

	// Proses Update Data
	case 'update':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Update Data
		try{
			$db->table($table)->where($primaryKey, $id)->update($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message,'data'=>$validData]);
	break;

	// Proses Delete
	case 'delete':
	try{
		$db->table($table)->where($primaryKey, $id)->delete();
		$status =  $db->commit();
	}catch(PDOException $e){
		// echo $e->getMessage();
		$status = true;
	}
	// Cetak Response Hasil Hapus Table
	echo $status ? 'Berhasil Dihapus' : 'Terjadi Kesalahan (Hapus) !';
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}