<?php
$status = true;

$table = 'class';
$primaryKey = 'class_id';
$joinQuery = "FROM `class` LEFT JOIN `major` USING(`major_id`) LEFT JOIN `teacher` USING(`teacher_id`)";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "*";

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'class_id', 'dt' => 'class_id'],
	['db' => 'class_room', 'dt' => 'class_room'],
	['db' => 'teacher_id', 'dt' => 'teacher_id'],
	['db' => 'teacher_name', 'dt' => 'teacher_name'],
	['db' => 'major_id', 'dt' => 'major_id'],
	['db' => 'major_name', 'dt' => 'major_name'],
	['db' => 'class_status', 'dt' => 'class_status' , 'formatter'=> function($val, $row){
		return $val=="1" ? 'Kelas X' : ($val=="2" ? 'Kelas XI' : 'Kelas XII');
	}],
];

// List Validasi
$validation = $v->make($_POST, [
	'class_room'  => 'required|min:3',
	'teacher_id'  => 'required',
	'major_id'  => 'required',
	'class_status'  => 'required',
]);

// Aliase Form Name
$validation->setAliases([
	'class_room' => 'Ruang Kelas',
	'teacher_id' => 'Guru',
	'major_id' => 'Jurusan',
	'class_status' => 'Status',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;
	
	// Cari Data
	case 'read':
	// Cetak Data Json
	echo json_encode($db->select($select)->table($table)->where($primaryKey,$id)->get());
	break;

	// Proses Create Data
	case 'create':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Tambah Data
		try{
			$db->table($table)->insert($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message]);
	break;
	
	// Proses Update Data
	case 'update':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Update Data
		try{
			$db->table($table)->where($primaryKey, $id)->update($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message,'data'=>$validData]);
	break;

	// Proses Delete
	case 'delete':
	try{
		$db->table($table)->where($primaryKey, $id)->delete();
		$status =  $db->commit();
	}catch(PDOException $e){
		// echo $e->getMessage();
		$status = true;
	}
	// Cetak Response Hasil Hapus Table
	echo $status ? 'Berhasil Dihapus' : 'Terjadi Kesalahan (Hapus) !';
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}
