<?php
$status = true;

$table = 'period';
$primaryKey = 'period_id';
$joinQuery = "";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "*";

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'period_id', 'dt' => 'period_id'],
	['db' => 'period_name', 'dt' => 'period_name'],
	['db' => 'period_status', 'dt' => 'period_status', 'formatter'=> function($val, $row){
		return $val=="1" ? 'Aktif' : 'Tidak Aktif';
	}],
];

// List Validasi
$validation = $v->make($_POST, [
	'period_name'  => 'required|min:3',
	'period_status'  => 'required',
]);

// Aliase Form Name
$validation->setAliases([
	'period_name' => 'Nama Periode',
	'period_status' => 'Status',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;
	
	// Cari Data
	case 'read':
	// Cetak Data Json
	echo json_encode($db->select($select)->table($table)->where($primaryKey,$id)->get());
	break;

	// Proses Create Data
	case 'create':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Tambah Data
		try{
			$db->table($table)->insert($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message]);
	break;
	
	// Proses Update Data
	case 'update':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Update Data
		try{
			$db->table($table)->where($primaryKey, $id)->update($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message,'data'=>$validData]);
	break;

	// Proses Delete
	case 'delete':
	try{
		$db->table($table)->where($primaryKey, $id)->delete();
		$status =  $db->commit();
	}catch(PDOException $e){
		// echo $e->getMessage();
		$status = true;
	}
	// Cetak Response Hasil Hapus Table
	echo $status ? 'Berhasil Dihapus' : 'Terjadi Kesalahan (Hapus) !';
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}