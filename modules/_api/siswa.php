<?php
$status = true;

$table = 'student';
$primaryKey = 'student_id';
$joinQuery = "FROM `student` LEFT JOIN `class` USING(`class_id`)";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "`student`.*, `users`.`username`";

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'student_id', 'dt' => 'student_id'],
	['db' => 'student_name', 'dt' => 'student_name'],
	['db' => 'student_gender', 'dt' => 'student_gender', 'formatter'=> function($val, $row){
		return $val=="1" ? 'Laki-Laki' : 'Perempuan';
	}],
	['db' => 'student_address', 'dt' => 'student_address'],
	// ['db' => 'class_id', 'dt' => 'class_id'],
	['db' => 'class_room', 'dt' => 'class_room'],
	['db' => 'student_phone1', 'dt' => 'student_phone1'],
	['db' => 'student_phone2', 'dt' => 'student_phone2'],
	['db' => 'student_phone3', 'dt' => 'student_phone3'],
];

// List Validasi
$validation = $v->make($_POST, [
	'student_id'  => 'default:NULL',
	'student_name'  => 'required|min:3',
	'student_gender'  => 'required',
	'student_address'  => 'required',
	'class_id'  => 'required',
	'student_phone1'  => 'required',
	'student_phone2'  => 'required',
	'student_phone3'  => 'required',
]);

// Aliase Form Name
$validation->setAliases([
	'student_id'  => 'NIS Murid',
	'student_name'  => 'Nama Murid',
	'student_gender'  => 'Jenis Kelamin',
	'student_address'  => 'Alamat',
	'class_id'  => 'Detail Murid',
	'student_phone1'  => 'No. Telepon 1',
	'student_phone2'  => 'No. Telepon 2',
	'student_phone3'  => 'No. Telepon 3',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;
	
	// Cari Data
	case 'read':
	// Cetak Data Json
	echo json_encode(
		$db->select($select)
		->table($table)
		->join('users',"student.student_id",'users.ref_id')
		->where("`users`.`users_type`",3)
		->where($primaryKey,$id)
		->get()
	);
	break;

	// Proses Create Data
	case 'create':

	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Tambah Data
		try{
			$db->table($table)->insert($validData);
			$status =  $db->commit();

			// Inputan Default By Sistem (Diinput Oleh Sistem)
			$DataUser['fullname']  = $validData['student_name'];
			$DataUser['username']  = $_POST['username'];
			$DataUser['password'] = isset($_POST['password']) ? md5($_POST['password']) : md5('12345678');
			$DataUser['users_type']  = '3';
			$DataUser['ref_id']  = $validData['student_id'];
			$db->table("users")->insert($DataUser);

		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message]);
	break;
	
	// Proses Update Data
	case 'update':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Update Data
		try{
			
			$id  = (int)$_POST['student_id_old'];

			$db->table($table)->where($primaryKey, $id)->update($validData);
			$status =  $db->commit();

			// Inputan Default By Sistem (Diinput Oleh Sistem)
			$DataUser['fullname']  = $validData['student_name'];
			$DataUser['username']  = $_POST['username'];
			$DataUser['password'] = isset($_POST['password']) ? md5($_POST['password']) : md5('12345678');
			$DataUser['users_type']  = '3';
			$DataUser['ref_id']  = $validData['student_id'];

			// Cek Password Diinput Atau Tidak?
			if ($_POST['password']==''):
				unset($DataUser['password']);
			else:
				$DataUser['password'] = md5($_POST['password']);
			endif;

			$db->table("users")
			->where("ref_id", $id)
			->where("users_type", 3)
			->update($DataUser);

		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message,'data'=>$validData]);
	break;

	// Proses Delete
	case 'delete':
	try{
		$db->table($table)->where($primaryKey, $id)->delete();
		$status =  $db->commit();

		$db->table("users")
		->where("ref_id", $id)
		->where("users_type", 3)
		->delete();
	}catch(PDOException $e){
		// echo $e->getMessage();
		$status = true;
	}
	// Cetak Response Hasil Hapus Table
	echo $status ? 'Berhasil Dihapus' : 'Terjadi Kesalahan (Hapus) !';
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}