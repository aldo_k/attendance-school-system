<?php
$status = true;

$table = 'teacher';
$primaryKey = 'teacher_id';
$joinQuery = "";
$extraWhere = "";
$groupBy = "";
$having = "";
$select = "`teacher`.*, `users`.`username`";

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'teacher_id', 'dt' => 'teacher_id'],
	['db' => 'teacher_name', 'dt' => 'teacher_name'],
	['db' => 'teacher_email', 'dt' => 'teacher_email'],
	['db' => 'teacher_phone', 'dt' => 'teacher_phone'],
	['db' => 'teacher_details', 'dt' => 'teacher_details'],
	['db' => 'teacher_gender', 'dt' => 'teacher_gender', 'formatter'=> function($val, $row){
		return $val=="1" ? 'Laki-Laki' : 'Perempuan';
	}],
];

// List Validasi
$validation = $v->make($_POST, [
	'teacher_name'  => 'required|min:3',
	'teacher_email'  => 'required',
	'teacher_phone'  => 'required',
	'teacher_details'  => 'required',
	'teacher_gender'  => 'required',
]);

// Aliase Form Name
$validation->setAliases([
	'teacher_name'  => 'Nama Guru',
	'teacher_email'  => 'Email',
	'teacher_phone'  => 'No. Telepon',
	'teacher_details'  => 'Detail Guru',
	'teacher_gender'  => 'Jenis Kelamin',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;
	
	// Cari Data
	case 'read':
	// Cetak Data Json
	echo json_encode(
		$db->select($select)
		->table($table)
		->join('users',"teacher.teacher_id",'users.ref_id')
		->where("`users`.`users_type`",2)
		->where($primaryKey,$id)
		->get()
	);
	break;

	// Proses Create Data
	case 'create':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Tambah Data
		try{
			$db->table($table)->insert($validData);
			$status =  $db->commit();

			// Inputan Default By Sistem (Diinput Oleh Sistem)
			$DataUser['fullname']  = $validData['teacher_name'];
			$DataUser['username']  = $_POST['username'];
			$DataUser['password'] = isset($_POST['password']) ? md5($_POST['password']) : md5('12345678');
			$DataUser['users_type']  = '2';
			$DataUser['ref_id']  = $db->insertId();
			$db->table("users")->insert($DataUser);

		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message]);
	break;
	
	// Proses Update Data
	case 'update':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Proses Update Data
		try{
			$db->table($table)->where($primaryKey, $id)->update($validData);
			$status =  $db->commit();

			// Inputan Default By Sistem (Diinput Oleh Sistem)
			$DataUser['fullname']  = $validData['teacher_name'];
			$DataUser['username']  = $_POST['username'];
			$DataUser['password'] = isset($_POST['password']) ? md5($_POST['password']) : md5('12345678');
			$DataUser['users_type']  = 2;
			$DataUser['ref_id']  = $id;

			// Cek Password Diinput Atau Tidak?
			if ($_POST['password']==''):
				unset($DataUser['password']);
			else:
				$DataUser['password'] = md5($_POST['password']);
			endif;

			$db->table("users")
			->where("ref_id", $id)
			->where("users_type", 2)
			->update($DataUser);

		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message,'data'=>$validData]);
	break;

	// Proses Delete
	case 'delete':
	try{
		$db->table($table)->where($primaryKey, $id)->delete();
		$status =  $db->commit();

		$db->table("users")
		->where("ref_id", $id)
		->where("users_type", 2)
		->delete();

	}catch(PDOException $e){
		// echo $e->getMessage();
		$status = true;
	}
	// Cetak Response Hasil Hapus Table
	echo $status ? 'Berhasil Dihapus' : 'Terjadi Kesalahan (Hapus) !';
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}