<?php
$status = true;

$table = 'users';
$primaryKey = 'users_id';
$joinQuery = "";
$extraWhere = "";
$groupBy = "";
$having = "";
// $select = "*";
$select = "`users_id`, `fullname`, `username`, `users_type`, `ref_id`";

// Deklarasi Kolom Response DataTables
$columns = [
	['db' => 'users_id', 'dt' => 'users_id'],
	['db' => 'fullname', 'dt' => 'fullname'],
	['db' => 'username', 'dt' => 'username'],
	['db' => 'users_type', 'dt' => 'users_type', 'formatter'=> function($val, $row){
		return $val=="1" ? 'Admin' : ($val=="2" ? 'Guru' : 'Wali Murid');
	}],
];

// List Validasi
$validation = $v->make($_POST, [
	'fullname'  => 'required',
	'username'  => 'required|min:5',
	'password'  => 'min:8',
]);

// Aliase Form Name
$validation->setAliases([
	'fullname' => 'Nama Lengkap',
	'username' => 'Username',
	'password' => 'Password',
]);

// Melakukan Validasi
$validation->validate();

// Mendapatkan Data Valid
$validData = $validation->getValidData();

// Default Messages Response
$message = [
	'csrf_token'=> [
		'status' => (bool)$isValidToken,
		'newToken'=> csrf_token(),
	],
	'form' => $validation->errors()->all()
];

$id  = isset($_GET[$primaryKey]) ? $_GET[$primaryKey] : '';
$act = isset($_GET['act']) ? $_GET['act'] : '';

// Cabang Aksi
switch ($act) {
	// Datatables Response
	case 'datatables':
	// Cetak Data Json
	echo json_encode(
		SSP::simple($_POST, $config['db'], $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having)
	);
	break;
	
	// Cari Data
	case 'read':
	// Cetak Data Json
	echo json_encode($db->select($select)->table($table)->where($primaryKey,$id)->get());
	break;

	// Proses Create Data
	case 'create':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):

		// Inputan Default By Sistem (Diinput Oleh Sistem)
		$validData['password'] = isset($validData['password']) ? md5($validData['password']) : md5('12345678');
		$validData['users_type']  = '1';
		$validData['ref_id']  = '0';

		// Proses Tambah Data
		try{
			$db->table($table)->insert($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message]);
	break;
	
	// Proses Update Data
	case 'update':
	// Cek Validasi Inputan
	if($isValidToken && !$validation->fails()):
		// Cek Password Diinput Atau Tidak?
		if ($_POST['password']==''):
			unset($validData['password']);
		else:
			$validData['password'] = md5($_POST['password']);
		endif;
		// Proses Update Data
		try{
			$db->table($table)->where($primaryKey, $id)->update($validData);
			$status =  $db->commit();
		}catch(PDOException $e){
			// echo $e->getMessage();
			$status = true;
		}
	endif;
	// Cetak Data Json
	echo json_encode(['error'=>$status,'message'=> $message,'data'=>$validData]);
	break;

	// Proses Delete
	case 'delete':
	try{
		$db->table($table)->where($primaryKey, $id)->delete();
		$status =  $db->commit();
	}catch(PDOException $e){
		// echo $e->getMessage();
		$status = true;
	}
	// Cetak Response Hasil Hapus Table
	echo $status ? 'Berhasil Dihapus' : 'Terjadi Kesalahan (Hapus) !';
	break;

	// Default Process
	default:
	// Cetak Data Json
	echo json_encode(['error'=>true,'messages'=> 'Nothing Action Founded']);
	break;
}
