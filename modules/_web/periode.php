<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="datatables-tab" data-toggle="pill" href="#data" role="tab"
						aria-controls="data"><i class="icon icon-list-alt"></i> Semua Periode</a>
					</li>
					<li>
						<a class="nav-link " id="v-pills-all-tab" data-toggle="pill" href="#form-tambah"
						role="tab" aria-controls="form-tambah"><i class="icon icon-plus-circle"></i>Tambah Periode</a>
					</li>
          <li id="edit-tab" style="display: none;">
            <a class="nav-link" id="v-pills-sellers-tab" data-toggle="pill" href="#form-edit" role="tab"
            aria-controls="form-edit"><i class="icon icon-edit"></i> Edit Data</a>
          </li>
          <!--  <li class="float-right">
            <a class="nav-link"  href="panel-page-users-create.html" ><i class="icon icon-plus-circle"></i> Add New User</a>
          </li> -->
        </ul>
      </div>
    </div>
  </header>
  <!-- Start Tab Content -->
  <div class="container-fluid animatedParent animateOnce">
  	<div class="tab-content my-3" id="v-pills-tabContent">
      <!-- Tab View Data Start -->
      <div class="tab-pane animated fadeInUpShort show active" id="data" role="tabpanel" aria-labelledby="v-pills-all-tab">
       <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header white">
              <i class="icon-list-alt blue-text"></i>
              <strong> Data Periode </strong>
              <button type="button" id="reload-data" class="btn btn-xs btn-primary r-5 float-right"><i class="icon-refresh"></i> Reload Data</button>
            </div>
            <div class="card-body">
              <div class="card-title"></div>
              <table class="table table-bordered table-hover nowarp" id="dataTable-SS"></table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Tab View Data End -->

    <!-- Tab Tambah Data Start -->
    <div class="tab-pane animated fadeInUpShort" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header white">
              Tambah Periode Baru
            </div>
            <div class="card-content">
              <div class="card-body">
                <div class="card-text" id="notification-tambah"></div>
                <form id="TambahForm" class="form" action="javascript:void(0);" method="post">
                  <?php echo csrf_field() ?>
                  <div class="form-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="tambah-period_name">Nama Periode </label>
                          <input id="tambah-period_name" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Periode" name="period_name" type="text" required="">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="tambah-period_status">Status </label>
                          <select id="tambah-period_status" class="form-control r-0 light s-12 select2-change" name="period_status" required="">
                            <option value="">Silahkan Pilih Status Periode</option>
                          </select>
                        </div>
                      </div>
                      <hr>
                    </div>
                  </div>
                  <div class="form-actions">
                    <hr>
                    <button class="btn btn-danger btn-sm mr-1 removeForm">
                      <i class="icon-arrow_back mr-2"></i> Kembali
                    </button>
                    <button type="button" class="btn btn-success btn-sm mr-1 tombol-tambah">
                      <i class="icon-save mr-2"></i> Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Tab Tambah Data End -->

    <!-- Tab Edit Data Start-->
    <div class="tab-pane animated fadeInUpShort" id="form-edit" role="tabpanel" aria-labelledby="v-pills-all-tab">
      <div class="row">
        <div class="col-md-8">
          <div class="card">
            <div class="card-header white">
              Ubah
            </div>
            <div class="card-content">
              <div class="card-body">
                <div class="card-text" id="notification-edit"></div>
                <form id="EditForm" class="form" action="javascript:void(0);" method="post">
                  <?php echo csrf_field() ?>
                  <div class="form-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="edit-period_name">Nama Periode </label>
                          <input id="edit-period_id" type="hidden" name="period_id">
                          <input id="edit-period_name" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Periode" name="period_name" type="text" required="">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="edit-period_status">Status </label>
                          <select id="edit-period_status" class="form-control r-0 light s-12 select2-change" name="period_status" required="">
                            <option value="">Silahkan Pilih Status Periode</option>
                          </select>
                        </div>
                      </div>
                      <hr>
                    </div>
                  </div>
                  <div class="form-actions">
                    <hr>
                    <button type="button" class="btn btn-danger btn-sm mr-1 removeForm">
                      <i class="icon-arrow_back mr-2"></i> Kembali
                    </button>
                    <button type="button" class="btn btn-success btn-sm mr-1 tombol-edit">
                      <i class="icon-save mr-2"></i> Simpan
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Tab Edit Data End-->
  </div>
</div>
<!-- End Tab Content -->
</div>
<script type="text/javascript">
	window.onload = function() {

    var idKey   = 'period_id';
    var apiPath = 'api.php?p=periode';

    $('#menu-name').html('Periode');
    $('.sidebar-menu').find('.menu-status').removeClass('active');
    $('.sidebar-menu').find('[data-menu="periode"]').addClass('active');
    
    // Definisi Kolom Table
    DtaoColumns = [
    { "data": idKey, "title": "No", "name": idKey,"render": function ( data, type, row, meta ) {
      return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "period_name", "title": "Nama Periode", "name": "period_name" },
    { "data": "period_status", "title": "Status", "name": "period_status" },
    { "data": idKey, "title": "Tindakan", "name": idKey, "render": function ( data, type, full ) {
      return '<a data-edit="'+full[idKey]+'" class="btn btn-sm btn-warning text-white" title="Ubah Data"><i class="icon-edit"></i></a> <button data-del="'+full[idKey]+'" data-load="" class="btn btn-sm btn-danger text-white" title="Hapus Data"><i class="icon-trash"></i></button>';
    }}
    ];

    // Definisi Lebar Kolom
    DtcolumnDefs = [
    { width: "20px", targets: 0 },
    { width: "50px", targets: 3 },
    ];

    var dataStatus = [{id:'1',text:'Aktif'},{id:'0',text:'Non-Aktif'}];

    $("#tambah-period_status").select2({
      data: dataStatus
    });

    $("#edit-period_status").select2({
      data: dataStatus
    });

    var table = $('#dataTable-SS').DataTable({
      "bProcessing": true,
      "language": {
        "decimal":        ",",
        "emptyTable":     "Data Tidak Tersedia...",
        "info":           "Tampil _START_ - _END_ dari _TOTAL_ Data",
        "infoEmpty":      "Tampil 0 to 0 of 0 Data",
        "infoFiltered":   "(filter dari _MAX_ total Data)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Tampil _MENU_ Data",
        "loadingRecords": "Loading...",
        "processing":     "Memperbarui Data",
        "searchPlaceholder": "Ketik Untuk Cari Data ...",
        "search":         "",
        "zeroRecords":    "Data Tidak Ditemukan",
        "paginate": {
          "first":      "Awal",
          "last":       "Akhir",
          "next":       "Lanjut",
          "previous":   "Kembali"
        }
      },
      "autoWidth": false,
      "ajax": {
        "url": apiPath + "&act=datatables",
        "type": "POST"
      },
      "bServerSide": true,
      "scrollX" : true,
      "scrollCollapse" : true,
      "aoColumns" : DtaoColumns,
      "columnDefs": DtcolumnDefs,
      "initComplete": function(settings, json) {
        // Tindakan Setelah Datatables Selesai Dijalankan
      }
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-tambah").click(function(){

      var $form = $("#TambahForm");
      var data = $form.serialize();

      $.ajax({
        type: 'POST',
        url:apiPath + `&act=create`,
        data: data,
        success: function(data) {

          data = $.parseJSON(data);
          $('input[name="_token"]').val(data.message.csrf_token.newToken);

          if(data.error==false){
            $type='success';
            $message = 'Tambah Data Berhasil';

            $('.removeForm').trigger('click');
            getData();
          } else {
            $type='error';
            $message = 'Tambah Data Gagal';
          }

          $info = '';
          $.each(data.message.form, function(index, val) {
            $info += (index+1) + ". " + val + "<br>";
          });

          Swal.fire({
            type: $type,
            title: $message,
            html: $info,
            timer: 1500 * (data.message.form.length + 1)
          });
        }
      });
    });

    // Aksi Open Form Edit
    $(document).on('click','[data-edit]',function(){

      var $param = $(this);
      var editId = $param.data('edit');

      $.get(apiPath + `&act=read&${idKey}=`+editId, function(data) {
        data = $.parseJSON(data);
        $.each(data, function(index, val) {
          $('#edit-'+index).val(val).trigger('change');
        });
      });
      $('#edit-tab').show();
      $('a[href="#form-edit"]').trigger('click');
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-edit").click(function(){

      var $form = $("#EditForm");
      var data = getFormData($form);
      var editId = data[idKey];
      var data = $form.serialize();

      $.ajax({
        type: 'POST',
        url:apiPath + `&act=update&${idKey}=`+editId,
        data: data,
        success: function(data) {

          data = $.parseJSON(data);

          $('input[name="_token"]').val(data.message.csrf_token.newToken);

          if(data.error==false){
            $type='success';
            $message = 'Ubah Data Berhasil';

            $('.removeForm').trigger('click');
            getData();
          } else {
            $type='error';
            $message = 'Ubah Data Gagal';
          }

          $info = '';
          $.each(data.message.form, function(index, val) {
            $info += (index+1) + ". " + val + "<br>";
          });

          Swal.fire({
            type: $type,
            title: $message,
            html: $info,
            timer: 1500 * (data.message.form.length + 1)
          });
        }
      });
    });

    // Aksi Hide Form Edit
    $(document).on('click','.removeForm',function(){
      $('#edit-tab').hide();
      $('a[href="#data"]').trigger('click');
      $('.form').trigger("reset");
      $('.form').trigger("change");
    });

    // Aksi Hapus
    $(document).on('click','[data-del]',function(){
      var $param = $(this);
      var deleteId = $param.data('del');

      Swal.fire({
        title: "Apakah Anda Yakin Menghapus Ingin Data?",
        text: "Jika Yakin, Silahkan Pilih Ya!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya, Hapus Sekarang',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonColor: '#d33',
        cancelButtonColor: '#333',
      }).then(isConfirm =>
      {
        if (isConfirm.value==true) {                       
          $.ajax({
            type: 'GET',    
            url: apiPath + "&act=delete",
            data: `${idKey}=`+deleteId,
            success: function(msg){
              Swal.fire("Terhapus!", msg, "success");
              getData();
            },
            error: function (request, kategori_sekolah, error) { 
              Swal.fire("Terjadi Kesalahan", request.responseText, "error");
            }
          });
        } else {
          Swal.fire("Dibatalkan", "Berhasil Membatalkan Penghapusan", "error");
        }
      })
    });


    function activityWatcher(){

      var secondsSinceLastActivity = 0;

      var maxInactivity = 10;

      setInterval(function(){
        secondsSinceLastActivity++;
        // if inactive >= 10 second
        if(secondsSinceLastActivity >= maxInactivity){
          // refresh data every 10 second
          (secondsSinceLastActivity%10)==0 ? getData() : '';
        }
      }, 1000);

      function activity(){
        secondsSinceLastActivity = 0;
      }

      var activityEvents = [
      'mousedown', 'mousemove', 'keydown',
      'scroll', 'touchstart'
      ];

      activityEvents.forEach(function(eventName) {
        document.addEventListener(eventName, activity, true);
      });
    }

    activityWatcher();

    function getData() {
      table.ajax.url(apiPath + "&act=datatables").load(null, true);
    }

    $('#reload-data').click(function(){
      getData();
    })
  }
</script>
