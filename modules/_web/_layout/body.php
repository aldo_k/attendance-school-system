<div class="container-fluid animatedParent animateOnce my-3">
	<div class="animated fadeInUpShort">
		<div class="card">
			<div class="card-header white">
				<h6> Selamat Datang Di Aplikasi Pencatatan Kehadiran Siswa SMK NEGERI 5 PALEMBANG </h6>
			</div>
			<div class="card-body">
				<div class="row text-justify">
					<div class="col-sm-5">
						<h3 class="text-center">Visi Sekolah</h3>
						<ul>
							<li>
								Unggul dalam persaingan global, Teknologi informasi dan komunikasi, dan berwawasan lingkungan.
							</li>
						</ul>
					</div>
					<div class="col-sm-7">
						<h3 class="text-center">Misi Sekolah</h3>
						<ol>
							<li>
								Menghasilkan tamatan yang mandiri, beriman dan bertaqwa terhadap tuhan yang maha Esa yang dilengkapi dengan pengetahuan dan keterampilan.
							</li>
							<li>
								Menyediakan layanan pendidikan yang unggul berbasis kewirausahaan dan teknologi informasi dan komunikasi.
							</li>
							<li>
								Meningkatkan kualitas sumber daya manusia agar dapat bersaingdi tingkat Nasional dan Internasiol.
							</li>
							<li>
								Meningkatkan kerjasama dengan dunia usaha dan industri  pelatihan kerja.
							</li>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
