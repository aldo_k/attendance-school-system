    <aside class="main-sidebar fixed offcanvas shadow">
      <section class="sidebar">
        <div class="mt-3 mb-3 ml-3">
          <!-- <img src="logo.png" alt=""> -->
          <h5>SMK NEGERI 5 PALEMBANG</h5>
        </div>
        <div class="relative">
          <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm fab-right fab-top btn-primary shadow1 ">
            <i class="icon icon-cogs"></i>
          </a>
          <div class="user-panel p-3 light mb-2">
            <div>
              <div class="float-left image">
                <img class="user_avatar" src="logo.png" alt="User Image">
              </div>
              <div class="float-left info">
                <h6 class="font-weight-light mt-2 mb-1"><?= $_SESSION['Fullname'] ?></h6>
                <i class="icon-circle text-success blink"></i> Online
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="collapse multi-collapse" id="userSettingsCollapse">
              <div class="list-group mt-3 shadow">
                <a href="?p=password" class="list-group-item list-group-item-action" id="password">
                  <i
                  class="mr-2 icon-vpn_key text-yellow"></i>Ubah Password
                </a>
                <a href="auth.php?p=logout" class="list-group-item list-group-item-action"><i
                  class="mr-2 icon-sign-out text-red"></i>Logout (Keluar)
                </a>
              </div>
            </div>
          </div>
        </div>
        <ul class="sidebar-menu">
          <li class="header"><strong>MAIN NAVIGATION</strong></li>
          <li class="menu-status" data-menu="beranda">
            <a href="?p=dashboard">
              <i class="icon icon-home2 s-18"></i> <span>Beranda</span>
            </a>
          </li>
          <li class="treeview menu-status" data-menu="jadwal">
            <a href="javascript:void(0)">
              <i class="icon icon-list-alt s-18"></i> <span>Jadwal KBM</span> <i class="icon icon-angle-left s-18 pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php $listPeriod = $db->select('period_id as id, period_name as text')->table('period')->where('period_status','1')->orderBy('id', 'desc')->getAll(); ?>
              <?php foreach ($listPeriod as $val) { ?>
                <li>
                  <a href="?p=jadwal&id=<?= $val->id ?>"><i class="icon icon-folder5"></i><?= $val->text ?></a>
                </li>
              <?php } ?>
            </ul>
          </li>
          <li class="treeview menu-status" data-menu="kehadiran">
            <a href="javascript:void(0)">
              <i class="icon icon-notebook-list s-18"></i> <span>Data Absensi</span> <i class="icon icon-angle-left s-18 pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <?php foreach ($listPeriod as $val) { ?>
                <li>
                  <a href="?p=kehadiran&id=<?= $val->id ?>"><i class="icon icon-folder5"></i><?= $val->text ?></a>
                </li>
              <?php } ?>
            </ul>
          </li>
          <?php if($_SESSION['Level']==1){ ?>
            <li class="header light mt-3"><strong>Data Master</strong></li>
            <li class="treeview menu-status" data-menu="guru_siswa">
              <a href="javascript:void(0)">
                <i class="icon icon-address-card-o s-18"></i> <span>Guru & Siswa</span>
                <i class="icon icon-angle-left s-18 pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="?p=siswa">
                    <i class="icon icon-address-book-o s-18"></i> <span>Siswa</span>
                  </a>
                </li>
                <li>
                  <a href="?p=guru">
                    <i class="icon icon-address-book-o s-18"></i> <span>Guru</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="menu-status" data-menu="admin">
              <a href="?p=admin">
                <i class="icon icon-supervisor_account s-18"></i> <span>Akun Pengguna</span>
              </a>
            </li>
            
            <li class="treeview menu-status" data-menu="jurusan_kelas">
              <a href="javascript:void(0)">
                <i class="icon icon-package s-18"></i> <span>Jurusan & Kelas</span>
                <i class="icon icon-angle-left s-18 pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="?p=kelas">
                    <i class="icon icon-notebook5 s-18"></i> <span>Kelas</span>
                  </a>
                </li>
                <li>
                  <a href="?p=jurusan">
                    <i class="icon icon-notebook5 s-18"></i> <span>Jurusan</span>
                  </a>
                </li>
              </ul>
            </li>

            <li class="menu-status" data-menu="periode">
              <a href="?p=periode">
                <i class="icon icon-calendar s-18"></i> <span>Periode</span>
              </a>
            </li>
          <?php } ?>
        </ul>
      </section>
    </aside>
    <!--Sidebar End-->
    <div class="page has-sidebar-left">
      <div class="navbar navbar-expand d-flex navbar-dark justify-content-between bd-navbar blue accent-3 shadow">
        <div class="relative">
          <div class="d-flex">
            <div>
              <a href="#" data-toggle="offcanvas" class="paper-nav-toggle pp-nav-toggle">
                <i></i>
              </a>
            </div>
            <div class="d-none d-md-block">
              <h1 class="nav-title text-white" id="menu-name"></h1>
            </div>
          </div>
        </div>
        <!--Top Menu Start -->
        <div class="navbar-custom-menu p-t-10">
          <ul class="nav navbar-nav">
            <li class="dropdown custom-dropdown user user-menu notifications-menu">
              <a href="#" class="nav-link" data-toggle="dropdown">
                <img src="logo.png" class="user-image" alt="User Image">
                <i class="icon-more_vert "></i>
              </a>
              <ul class="dropdown-menu" style="width:180px">
                <li class="header">Hai, <?= $_SESSION['Fullname'] ?></li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="?p=password">
                        <i class="icon icon-vpn_key"></i> Ubah Password
                      </a>
                    </li>
                    <li>
                      <a href="auth.php?p=logout">
                        <i class="icon icon-sign-out"></i> Logout (Keluar)
                      </a>
                    </li>
                  </ul>
                </li>
                <!-- <li class="footer p-2 text-center"><a href="#">View all</a></li> -->
              </ul>
            </li>
          </ul>
        </div>
      </div>
