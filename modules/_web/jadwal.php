<?php $period = $db->select('*')->table('period')->where('period_id',(int)@$_GET['id'])->get(); ?>
<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="datatables-tab" data-toggle="pill" href="#data" role="tab"
						aria-controls="data"><i class="icon icon-list-alt"></i> Semua Jadwal KBM</a>
					</li>
          <?php if($_SESSION['Level']==1){ ?>
            <li>
              <a class="nav-link " id="form-tambah-tab" data-toggle="pill" href="#form-tambah"
              role="tab" aria-controls="form-tambah"><i class="icon icon-plus-circle"></i>Tambah Jadwal KBM</a>
            </li>
            <li id="edit-tab" style="display: none;">
              <a class="nav-link" id="v-pills-sellers-tab" data-toggle="pill" href="#form-edit" role="tab"
              aria-controls="form-edit"><i class="icon icon-edit"></i> Edit Data</a>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </header>
  <!-- Start Tab Content -->
  <div class="container-fluid animatedParent animateOnce">
   <div class="tab-content my-3" id="v-pills-tabContent">
    <!-- Tab View Data Start -->
    <div class="tab-pane animated fadeInUpShort show active" id="data" role="tabpanel" aria-labelledby="v-pills-all-tab">
     <div class="row">
      <div class="col-md-12">
       <div class="card">
        <div class="card-header white">
          <i class="icon-list-alt blue-text"></i>
          <strong> Data Jadwal KBM <?= $period->period_name ?></strong>
          <button type="button" class="btn btn-xs btn-primary r-5 float-right reload-data"><i class="icon-refresh"></i> Reload Data</button>
        </div>
        <div class="card-body">
          <div class="card-title">
            <div class="row form-inline">
             <div class="col-sm-12">
              <div class="form-group">
                <?php if($_SESSION['Level']==3) { ?>
                  <div class="col-sm-5">
                  <?php } else { ?>
                    <div class="col-sm-4">
                    <?php }?>
                    <select id="form-class_id" class="form-control r-0 light s-12" name="class_id" required="">
                      <?php if($_SESSION['Level']==1){ ?>
                        <option value="all">Semua Kelas </option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php if($_SESSION['Level']!=3) { ?>
                    <div class="col-sm-4">
                      <select id="form-teacher_id" class="form-control r-0 light s-12" name="teacher_id" required="">
                        <?php if($_SESSION['Level']==1){ ?>
                          <option value="all">Semua Guru</option>
                        <?php } ?>
                      </select>
                    </div>
                  <?php } ?>
                  <?php if($_SESSION['Level']==3) { ?>
                    <div class="col-sm-5">
                      <select id="form-schedule_day" class="form-control r-0 light s-12" name="schedule_day" required="">
                        <option value="all">Semua Hari</option>
                      </select>
                    </div>
                  <?php } else { ?>
                    <div class="col-sm-2">
                      <select id="form-schedule_day" class="form-control r-0 light s-12" name="schedule_day" required="">
                        <option value="all">Semua Hari</option>
                      </select>
                    </div>
                  <?php } ?>
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-sm btn-success r-5 reload-data"><i class="icon icon-search"></i>Filter Jadwal</button>
                  </div>
                </div>
              </div>
            </div>
            <hr>
          </div>
          <table class="table table-bordered table-hover nowarp" id="dataTable-SS"></table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Tab View Data End -->
<?php if($_SESSION['Level']==1){ ?>
  <!-- Tab Tambah Data Start -->
  <div class="tab-pane animated fadeInUpShort" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
   <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header white">
          Tambah Jadwal KBM Baru
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="card-text" id="notification-tambah"></div>
            <form id="TambahForm" class="form" action="javascript:void(0);" method="post">
             <?php echo csrf_field() ?>
             <div class="form-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-period_id">Periode </label>
                    <select id="tambah-period_id" class="form-control r-0 light s-12 select2-change" name="period_id" required="">
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-teacher_id">Guru </label>
                    <select id="tambah-teacher_id" class="form-control r-0 light s-12 select2-change" name="teacher_id" required="">
                      <option value="">Silahkan Pilih Status Jadwal KBM</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-class_id">Kelas</label>
                    <select id="tambah-class_id" class="form-control r-0 light s-12 select2-change" name="class_id" required="">
                      <option value="">Silahkan Pilih Kelas</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-schedule_day">Hari </label>
                    <select id="tambah-schedule_day" class="form-control r-0 light s-12 select2-change" name="schedule_day" required="">
                      <option value="">Silahkan Pilih Hari</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-schedule_starttime">Jam Mulai </label>
                    <input id="tambah-schedule_starttime" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_starttime" type="text" required="">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-schedule_endtime">Jam Selesai </label>
                    <input id="tambah-schedule_endtime" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_endtime" type="text" required="">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-schedule_code">Kode Jadwal KBM </label>
                    <input id="tambah-schedule_code" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_code" type="text" required="">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="tambah-schedule_name">Nama Mata Pelajaran </label>
                    <input id="tambah-schedule_name" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_name" type="text" required="">
                  </div>
                </div>
                <hr>
              </div>
            </div>
            <div class="form-actions">
              <hr>
              <button class="btn btn-danger btn-sm mr-1 removeForm">
                <i class="icon-arrow_back mr-2"></i> Kembali
              </button>
              <button type="button" class="btn btn-success btn-sm mr-1 tombol-tambah">
               <i class="icon-save mr-2"></i> Simpan
             </button>
           </div>
         </form>
       </div>
     </div>
   </div>
 </div>
 <div class="col-md-8">
   <div class="card">
    <div class="card-header white">
      <i class="icon-list-alt blue-text"></i>
      <strong> List View Jadwal KBM Saat Ini</strong>
    </div>
    <div class="card-body">
      <div class="card-title" id="infoDataTambah"></div>
      <table class="table table-bordered table-hover nowarp" id="Dt-add"></table>
    </div>
  </div>
</div>
</div>
</div>
<!-- Tab Tambah Data End -->

<!-- Tab Edit Data Start-->
<div class="tab-pane animated fadeInUpShort" id="form-edit" role="tabpanel" aria-labelledby="v-pills-all-tab">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header white">
          Ubah
        </div>
        <div class="card-content">
          <div class="card-body">
            <div class="card-text" id="notification-edit"></div>
            <form id="EditForm" class="form" action="javascript:void(0);" method="post">
              <?php echo csrf_field() ?>
              <div class="form-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-period_id">Periode </label>
                      <input id="edit-schedule_id" type="hidden" name="schedule_id">
                      <select id="edit-period_id" class="form-control r-0 light s-12 select2-change" name="period_id" required="">
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-teacher_id">Guru </label>
                      <select id="edit-teacher_id" class="form-control r-0 light s-12 select2-change" name="teacher_id" required="">
                        <option value="">Silahkan Pilih Status Jadwal KBM</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-class_id">Kelas</label>
                      <select id="edit-class_id" class="form-control r-0 light s-12 select2-change" name="class_id" required="">
                        <option value="">Silahkan Pilih Kelas</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-schedule_day">Hari </label>
                      <select id="edit-schedule_day" class="form-control r-0 light s-12 select2-change" name="schedule_day" required="">
                        <option value="">Silahkan Pilih Hari</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-schedule_starttime">Jam Mulai </label>
                      <input id="edit-schedule_starttime" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_starttime" type="text" required="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-schedule_endtime">Jam Selesai </label>
                      <input id="edit-schedule_endtime" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_endtime" type="text" required="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-schedule_code">Kode Jadwal KBM </label>
                      <input id="edit-schedule_code" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_code" type="text" required="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="edit-schedule_name">Nama Mata Pelajaran </label>
                      <input id="edit-schedule_name" class="form-control r-0 light s-12 " placeholder="Masukkan Nama Jadwal KBM" name="schedule_name" type="text" required="">
                    </div>
                  </div>
                  <hr>
                </div>
              </div>
              <div class="form-actions">
                <hr>
                <button type="button" class="btn btn-danger btn-sm mr-1 removeForm">
                  <i class="icon-arrow_back mr-2"></i> Kembali
                </button>
                <button type="button" class="btn btn-success btn-sm mr-1 tombol-edit">
                  <i class="icon-save mr-2"></i> Simpan
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-8">
     <div class="card">
      <div class="card-header white">
        <i class="icon-list-alt blue-text"></i>
        <strong> List View Jadwal KBM Saat Ini</strong>
      </div>
      <div class="card-body">
        <div class="card-title" id="infoDataTambah"></div>
        <table class="table table-bordered table-hover nowarp" id="Dt-edit"></table>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Tab Edit Data End-->
<?php } ?>
</div>
</div>
<!-- End Tab Content -->
</div>
<script type="text/javascript">
	window.onload = function() {

		var idKey   = 'schedule_id';
		var apiPath = 'api.php?p=jadwal';

		$('#menu-name').html('Jadwal KBM');
		$('.sidebar-menu').find('.menu-status').removeClass('active');
		$('.sidebar-menu').find('[data-menu="jadwal"]').addClass('active');

    var TodayId = <?= date("N") ?>;

    // Definisi Kolom Table
    DtaoColumns = [
    { "data": idKey, "title": "No", "name": idKey,"render": function ( data, type, row, meta ) {
    	return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "class_room", "title": "Kelas", "name": "class_room" },
    { "data": "teacher_name", "title": "Guru", "name": "teacher_name" },
    { "data": "schedule_code", "title": "Kode KBM", "name": "schedule_code" },
    { "data": "schedule_name", "title": "Mata Pelajaran", "name": "schedule_name" },
    { "data": "schedule_day", "title": "Hari", "name": "schedule_day", "render": function ( data, type, full ) {
    	return dataDays[(full['schedule_day']-1)]['text'] + ', ' + full['schedule_starttime'] + ' s/d ' + full['schedule_endtime'];
    }},
    <?php if($_SESSION['Level']==1){ ?>
      { "data": idKey, "title": "Tindakan", "name": idKey, "render": function ( data, type, full ) {
       return '<a data-edit="'+full[idKey]+'" class="btn btn-sm btn-warning text-white" title="Ubah Data"><i class="icon-edit"></i></a> <button data-del="'+full[idKey]+'" data-load="" class="btn btn-sm btn-danger text-white" title="Hapus Data"><i class="icon-trash"></i></button>';
     }}
   <?php } else if($_SESSION['Level']==2){?>
    { "data": "schedule_day", "title": "Tindakan", "name": "schedule_day", "render": function ( data, type, full ) {
      if (data==TodayId) {
       return '<a href="?p=kehadiran_siswa&id=<?= (int)@$_GET['id'] ?>&class_id='+full['class_id']+'" class="btn btn-xs btn-success text-white" title="Lakukan Absensi"><i class="icon-clipboard-list"></i> Lakukan Absensi</a>';
     } else {
      return '<a href="javascript:void(0)" onclick="return swal.fire({type: \'error\',title:\'Tidak Diizinkan\',html: \'Absensi Hanya Bisa Dilakukan Pada Hari '+dataDays[(full['schedule_day']-1)]['text']+'\' })" class="btn btn-xs btn-danger text-white"><i class="icon-lock"></i> Absensi Terkunci</a>';
    }
  }}
<?php } ?>
];

    // Definisi Kolom Table
    DtaoColumnsAdd = [
    { "data": idKey, "title": "No", "name": idKey,"render": function ( data, type, row, meta ) {
      return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "class_room", "title": "Kelas", "name": "class_room" },
    { "data": "teacher_name", "title": "Guru", "name": "teacher_name" },
    { "data": "schedule_code", "title": "Kode KBM", "name": "schedule_code" },
    { "data": "schedule_day", "title": "Hari", "name": "schedule_day", "render": function ( data, type, full ) {
      return dataDays[(full['schedule_day']-1)]['text'] + ', ' + full['schedule_starttime'] + ' s/d ' + full['schedule_endtime'];
    }},
    { "data": "schedule_name", "title": "Mata Pelajaran", "name": "schedule_name" },
    ];

    // Definisi Lebar Kolom
    DtcolumnDefs = [
    { width: "20px", targets: 0 },
    <?php if($_SESSION['Level']==1){ ?>
      { width: "50px", targets: 6 },
    <?php } ?>
    ];

    // Definisi Lebar Kolom
    DtcolumnDefsAdd = [
    { width: "20px", targets: 0 },
    ];
    <?php 
    if($_SESSION['Level']==2){
      $hari = date("N");
      $class = $db->select('class.class_id as id, class.class_room as text, major_name as grup')
      ->table('class')
      ->join('`schedule`','`schedule`.class_id','class.class_id')
      ->join('major','major.major_id','class.major_id')
      ->where("`schedule`.teacher_id",$_SESSION['Reference'])
      ->where("`schedule`.schedule_day",$hari)
      ->getAll();
    } else if($_SESSION['Level']==1){
      $class = $db->select('class_id as id, class_room as text, major_name as grup')->table('class')->join('major','major.major_id','class.major_id')->getAll();
    } else {
      $class = $db->select('student.class_id as id, class_room as text, major_name as grup')->table('class')
      ->join('major','major.major_id','class.major_id')
      ->join('student','student.class_id','class.major_id')
      ->where("student_id",$_SESSION['Reference'])
      ->getAll();
    }

    $data = array_reduce($class, function(array $grup, $list){
    	$grup[$list->grup][] = ['id'=>$list->id,'text'=>$list->text];
    	return $grup;
    },[]);
    $classGroup = array();
    foreach ($data as $key => $value) {
    	$classGroup[] = [ 'text'=> "[ Jurusan $key ]", 'children'=> $value ];
    }

    ?>

    var dataDays = [{id:'1',text:'Senin'},{id:'2',text:'Selasa'},{id:'3',text:'Rabu'},{id:'4',text:'Kamis'},{id:'5',text:'Jum`at'},{id:'6',text:'Sabtu'},{id:'7',text:'Minggu'}];

    var dataClass = <?= json_encode($classGroup); ?>;

    <?php if($_SESSION['Level']==2){ ?>
      var dataTeacher = <?= json_encode($db->select('teacher_id as id, teacher_name as text')->table('teacher')->where('teacher_id',$_SESSION['Reference'])->getAll()) ?>;
    <?php } else if($_SESSION['Level']==1) { ?>
      var dataTeacher = <?= json_encode($db->select('teacher_id as id, teacher_name as text')->table('teacher')->getAll()) ?>;
    <?php } ?>
    var dataPeriod = <?= json_encode($listPeriod) ?>;

    $("#form-class_id").select2({
    	data: dataClass
    });

    <?php if($_SESSION['Level']!=3) { ?>
      $("#form-teacher_id").select2({
       data: dataTeacher
     });
    <?php } ?>

    $("#form-schedule_day").select2({
     data: dataDays
   });

    $("#tambah-schedule_day").select2({
     data: dataDays
   });

    $("#tambah-class_id").select2({
      data: dataClass
    });

    <?php if($_SESSION['Level']!=3) { ?>
      $("#tambah-teacher_id").select2({
        data: dataTeacher
      });
    <?php } ?>

    $("#tambah-period_id").select2({
      data: dataPeriod
    });

    $("#edit-schedule_day").select2({
     data: dataDays
   });

    $("#edit-class_id").select2({
      data: dataClass
    });

    <?php if($_SESSION['Level']!=3) { ?>
      $("#edit-teacher_id").select2({
        data: dataTeacher
      });
    <?php } ?>

    $("#edit-period_id").select2({
      data: dataPeriod
    });

    var Dtlanguage = {
      "decimal":        ",",
      "emptyTable":     "Data Tidak Tersedia...",
      "info":           "Tampil _START_ - _END_ dari _TOTAL_ Data",
      "infoEmpty":      "Tampil 0 to 0 of 0 Data",
      "infoFiltered":   "(filter dari _MAX_ total Data)",
      "infoPostFix":    "",
      "thousands":      ".",
      "lengthMenu":     "Tampil _MENU_ Data",
      "loadingRecords": "Loading...",
      "processing":     "Memperbarui Data",
      "searchPlaceholder": "Ketik Untuk Cari Data ...",
      "search":         "",
      "zeroRecords":    "Data Tidak Ditemukan",
      "paginate": {
        "first":      "Awal",
        "last":       "Akhir",
        "next":       "Lanjut",
        "previous":   "Kembali"
      }
    };

    // Table Data
    var table = $('#dataTable-SS').DataTable({
       <?php if($_SESSION['Level']!=1) { ?>
        "searching": false,
        "paging": false,
        "pageLength": 100,
      <?php } ?>
      "bProcessing": true,
      "language": Dtlanguage,
      "autoWidth": false,
      "ajax": {
        "url": apiPath + "&act=datatables",
        "type": "POST",
        data: function ( d ) {
         return $.extend( {}, d, {
          "period_id": "<?= (int)$_GET['id'] ?>",
          "class_id": $("#form-class_id").val(),
          "teacher_id": $("#form-teacher_id").val(),
          "schedule_day": $("#form-schedule_day").val(),
        } );
       }
     },
     "bServerSide": true,
     "scrollX" : true,
     "scrollCollapse" : true,
     "aoColumns" : DtaoColumns,
     "columnDefs": DtcolumnDefs,
     "initComplete": function(settings, json) {
        // Tindakan Setelah Datatables Selesai Dijalankan
      }
    });

    // Table Tambah
    var TbAddForm = $('#Dt-add').DataTable({
      "bProcessing": true,
      "language": Dtlanguage,
      "autoWidth": false,
      "ajax": {
        "url": apiPath + "&act=datatables",
        "type": "POST",
        data: function ( d ) {
          return $.extend( {}, d, {
            "period_id": $("#tambah-period_id").val(),
            "class_id": $("#tambah-class_id").val(),
            "teacher_id": $("#tambah-teacher_id").val(),
            "schedule_day": $("#tambah-schedule_day").val(),
          } );
        }
      },
      "bServerSide": true,
      "scrollX" : true,
      "scrollCollapse" : true,
      "aoColumns" : DtaoColumnsAdd,
      "columnDefs": DtcolumnDefsAdd,
      "initComplete": function(settings, json) {
        $('#form-tambah-tab').on('click', function(event) {
          setTimeout(function(){ TbAddForm.ajax.url(apiPath + "&act=datatables").load(null, true); }, 100);
        });
      }
    });

    // Table Tambah
    var TbEditForm = $('#Dt-edit').DataTable({
      "bProcessing": true,
      "language": Dtlanguage,
      "autoWidth": false,
      "ajax": {
        "url": apiPath + "&act=datatables",
        "type": "POST",
        data: function ( d ) {
          return $.extend( {}, d, {
            "period_id": $("#edit-period_id").val(),
            "class_id": $("#edit-class_id").val(),
            "teacher_id": $("#edit-teacher_id").val(),
            "schedule_day": $("#edit-schedule_day").val(),
          } );
        }
      },
      "bServerSide": true,
      "scrollX" : true,
      "scrollCollapse" : true,
      "aoColumns" : DtaoColumnsAdd,
      "columnDefs": DtcolumnDefsAdd,
      "initComplete": function(settings, json) {

      }
    });

    $('#TambahForm').on('change', function(event) {
      TbAddForm.ajax.url(apiPath + "&act=datatables").load(null, true);
    });

    var OnPageEdit = false;
    $('#EditForm').on('change', function(event) {
      if (OnPageEdit==true) {
        TbEditForm.ajax.url(apiPath + "&act=datatables").load(null, true);
      }
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-tambah").click(function(){

    	var $form = $("#TambahForm");
    	var data = $form.serialize();

    	$.ajax({
    		type: 'POST',
    		url:apiPath + `&act=create`,
    		data: data,
    		success: function(data) {

    			data = $.parseJSON(data);
    			$('input[name="_token"]').val(data.message.csrf_token.newToken);

    			if(data.error==false){
    				$type='success';
    				$message = 'Tambah Data Berhasil';

    				$('.removeForm').trigger('click');
    				getData();
    			} else {
    				$type='error';
    				$message = 'Tambah Data Gagal';
    			}

    			$info = '';
    			$.each(data.message.form, function(index, val) {
    				$info += (index+1) + ". " + val + "<br>";
    			});

    			Swal.fire({
    				type: $type,
    				title: $message,
    				html: $info,
    				timer: 1500 * (data.message.form.length + 1)
    			});
    		}
    	});
    });

    // Aksi Open Form Edit
    $(document).on('click','[data-edit]',function(){

    	var $param = $(this);
    	var editId = $param.data('edit');

    	$.get(apiPath + `&act=read&${idKey}=`+editId, function(data) {
    		data = $.parseJSON(data);
    		$.each(data, function(index, val) {
    			$('#edit-'+index).val(val).trigger('change');
    		});
    	});
    	$('#edit-tab').show();
    	$('a[href="#form-edit"]').trigger('click');

      setTimeout(function(){ 
        OnPageEdit = true;
        TbEditForm.ajax.url(apiPath + "&act=datatables").load(null, true);
      }, 200);
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-edit").click(function(){

    	var $form = $("#EditForm");
    	var data = getFormData($form);
    	var editId = data[idKey];
    	var data = $form.serialize();

    	$.ajax({
    		type: 'POST',
    		url:apiPath + `&act=update&${idKey}=`+editId,
    		data: data,
    		success: function(data) {

    			data = $.parseJSON(data);

    			$('input[name="_token"]').val(data.message.csrf_token.newToken);

    			if(data.error==false){
    				$type='success';
    				$message = 'Ubah Data Berhasil';

    				$('.removeForm').trigger('click');
    				getData();
    			} else {
    				$type='error';
    				$message = 'Ubah Data Gagal';
    			}

    			$info = '';
    			$.each(data.message.form, function(index, val) {
    				$info += (index+1) + ". " + val + "<br>";
    			});

    			Swal.fire({
    				type: $type,
    				title: $message,
    				html: $info,
    				timer: 1500 * (data.message.form.length + 1)
    			});
    		}
    	});
    });

    // Aksi Hide Form Edit
    $(document).on('click','.removeForm',function(){
    	$('#edit-tab').hide();
    	$('a[href="#data"]').trigger('click');
    	$('.form').trigger("reset");
      $('.form').trigger("change");
      setTimeout(function(){ 
        OnPageEdit = false;
        table.ajax.url(apiPath + "&act=datatables").load(null, true);
      }, 200);
    });

    // Aksi Hapus
    $(document).on('click','[data-del]',function(){
    	var $param = $(this);
    	var deleteId = $param.data('del');

    	Swal.fire({
    		title: "Apakah Anda Yakin Menghapus Ingin Data?",
    		text: "Jika Yakin, Silahkan Pilih Ya!",
    		type: 'warning',
    		showCancelButton: true,
    		confirmButtonText: 'Ya, Hapus Sekarang',
    		cancelButtonText: 'Tidak, Batalkan!',
    		confirmButtonColor: '#d33',
    		cancelButtonColor: '#333',
    	}).then(isConfirm =>
    	{
    		if (isConfirm.value==true) {                       
    			$.ajax({
    				type: 'GET',    
    				url: apiPath + "&act=delete",
    				data: `${idKey}=`+deleteId,
    				success: function(msg){
    					Swal.fire("Terhapus!", msg, "success");
    					getData();
    				},
    				error: function (request, kategori_sekolah, error) { 
    					Swal.fire("Terjadi Kesalahan", request.responseText, "error");
    				}
    			});
    		} else {
    			Swal.fire("Dibatalkan", "Berhasil Membatalkan Penghapusan", "error");
    		}
    	})
    });


    function activityWatcher(){

    	var secondsSinceLastActivity = 0;

    	var maxInactivity = 10;

    	setInterval(function(){
    		secondsSinceLastActivity++;
    		// if inactive >= 10 second
    		if(secondsSinceLastActivity >= maxInactivity){
    			// refresh data every 10 second
    			(secondsSinceLastActivity%10)==0 ? getData() : '';
          (secondsSinceLastActivity%10)==0 ? getDataAdd() : '';
        }
      }, 1000);

    	function activity(){
    		secondsSinceLastActivity = 0;
    	}

    	var activityEvents = [
    	'mousedown', 'mousemove', 'keydown',
    	'scroll', 'touchstart'
    	];

    	activityEvents.forEach(function(eventName) {
    		document.addEventListener(eventName, activity, true);
    	});
    }

    activityWatcher();

    function getData() {
    	table.ajax.url(apiPath + "&act=datatables").load(null, true);
    }

    function getDataAdd() {
      TbAddForm.ajax.url(apiPath + "&act=datatables").load(null, true);
    }

    $('.reload-data').click(function(){
    	getData();
    })
  }
</script>