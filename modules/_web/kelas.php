<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="datatables-tab" data-toggle="pill" href="#data" role="tab"
						aria-controls="data"><i class="icon icon-list-alt"></i> Semua Kelas</a>
					</li>
					<li>
						<a class="nav-link " id="v-pills-all-tab" data-toggle="pill" href="#form-tambah"
						role="tab" aria-controls="form-tambah"><i class="icon icon-plus-circle"></i>Tambah Kelas</a>
					</li>
          <li id="edit-tab" style="display: none;">
            <a class="nav-link" id="v-pills-sellers-tab" data-toggle="pill" href="#form-edit" role="tab"
            aria-controls="form-edit"><i class="icon icon-edit"></i> Edit Data</a>
          </li>
          <!--  <li class="float-right">
            <a class="nav-link"  href="panel-page-users-create.html" ><i class="icon icon-plus-circle"></i> Add New User</a>
          </li> -->
        </ul>
      </div>
    </div>
  </header>
  <!-- Start Tab Content -->
  <div class="container-fluid animatedParent animateOnce">
  	<div class="tab-content my-3" id="v-pills-tabContent">
      <!-- Tab View Data Start -->
      <div class="tab-pane animated fadeInUpShort show active" id="data" role="tabpanel" aria-labelledby="v-pills-all-tab">
       <div class="row">
        <div class="col-md-12">
         <div class="card">
          <div class="card-header white">
            <i class="icon-list-alt blue-text"></i>
            <strong> Data Kelas </strong>
            <button type="button" id="reload-data" class="btn btn-xs btn-primary r-5 float-right"><i class="icon-refresh"></i> Reload Data</button>
          </div>
          <div class="card-body">
            <div class="card-title"></div>
            <table class="table table-bordered table-hover nowarp" id="dataTable-SS"></table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Tab View Data End -->

  <!-- Tab Tambah Data Start -->
  <div class="tab-pane animated fadeInUpShort" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header white">
            Tambah Kelas Baru
          </div>
          <div class="card-content">
            <div class="card-body">
              <div class="card-text" id="notification-tambah"></div>
              <form id="TambahForm" class="form" action="javascript:void(0);" method="post">
                <?php echo csrf_field() ?>
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tambah-major_id">Jurusan </label>
                        <select id="tambah-major_id" class="form-control r-0 light s-12" name="major_id" required="">
                          <option value="">Silahkan Pilih Jurusan</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tambah-class_status">Tingkat Kelas </label>
                        <select id="tambah-class_status" class="form-control r-0 light s-12" name="class_status" required="">
                          <option value="">Silahkan Pilih Tingkat Kelas</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tambah-class_room">Ruang Kelas </label>
                        <input id="tambah-class_room" class="form-control r-0 light s-12 " placeholder="Masukkan Ruang Kelas" name="class_room" type="text" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="tambah-teacher_id">Wali Kelas </label>
                        <select id="tambah-teacher_id" class="form-control r-0 light s-12" name="teacher_id" required="">
                          <option value="">Silahkan Pilih Wali Kelas</option>
                        </select>
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                <div class="form-actions">
                  <hr>
                  <button class="btn btn-danger btn-sm mr-1 removeForm">
                    <i class="icon-arrow_back mr-2"></i> Kembali
                  </button>
                  <button type="button" class="btn btn-success btn-sm mr-1 tombol-tambah">
                    <i class="icon-save mr-2"></i> Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Tab Tambah Data End -->

  <!-- Tab Edit Data Start-->
  <div class="tab-pane animated fadeInUpShort" id="form-edit" role="tabpanel" aria-labelledby="v-pills-all-tab">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header white">
            Ubah
          </div>
          <div class="card-content">
            <div class="card-body">
              <div class="card-text" id="notification-edit"></div>
              <form id="EditForm" class="form" action="javascript:void(0);" method="post">
                <?php echo csrf_field() ?>
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edit-major_id">Jurusan </label>
                        <select id="edit-major_id" class="form-control r-0 light s-12 select2-change" name="major_id" required="">
                          <option value="">Silahkan Pilih Jurusan</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edit-class_status">Tingkat Kelas </label>
                        <select id="edit-class_status" class="form-control r-0 light s-12 select2-change" name="class_status" required="">
                          <option value="">Silahkan Pilih Tingkat Kelas</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edit-class_room">Ruang Kelas </label>
                        <input id="edit-class_id" type="hidden" name="class_id">
                        <input id="edit-class_room" class="form-control r-0 light s-12 " placeholder="Masukkan Ruang Kelas" name="class_room" type="text" required="">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="edit-teacher_id">Wali Kelas </label>
                        <select id="edit-teacher_id" class="form-control r-0 light s-12 select2-change" name="teacher_id" required="">
                          <option value="">Silahkan Pilih Wali Kelas</option>
                        </select>
                      </div>
                    </div>
                    <hr>
                  </div>
                </div>
                <div class="form-actions">
                  <hr>
                  <button type="button" class="btn btn-danger btn-sm mr-1 removeForm">
                    <i class="icon-arrow_back mr-2"></i> Kembali
                  </button>
                  <button type="button" class="btn btn-success btn-sm mr-1 tombol-edit">
                    <i class="icon-save mr-2"></i> Simpan
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Tab Edit Data End-->
</div>
</div>
<!-- End Tab Content -->
</div>
<script type="text/javascript">
	window.onload = function() {

    var idKey   = 'class_id';
    var apiPath = 'api.php?p=kelas';

    $('#menu-name').html('Kelas');
    $('.sidebar-menu').find('.menu-status').removeClass('active');
    $('.sidebar-menu').find('[data-menu="jurusan_kelas"]').addClass('active');
    
    // Definisi Kolom Table
    DtaoColumns = [
    { "data": idKey, "title": "No", "name": idKey,"render": function ( data, type, row, meta ) {
      return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "class_status", "title": "Status Kelas", "name": "class_status" },
    { "data": "class_room", "title": "Ruang Kelas", "name": "class_room" },
    { "data": "teacher_name", "title": "Wali Kelas", "name": "teacher_name" },
    { "data": "major_name", "title": "Jurusan", "name": "major_name" },
    // { "data": "teacher_id", "title": "Wali Kelas", "name": "teacher_id" },
    // { "data": "major_id", "title": "Jurusan", "name": "major_id" },
    { "data": idKey, "title": "Tindakan", "name": idKey, "render": function ( data, type, full ) {
      return '<a data-edit="'+full[idKey]+'" class="btn btn-sm btn-warning text-white" title="Ubah Data"><i class="icon-edit"></i></a> <button data-del="'+full[idKey]+'" data-load="" class="btn btn-sm btn-danger text-white" title="Hapus Data"><i class="icon-trash"></i></button>';
    }}
    ];

    // Definisi Lebar Kolom
    DtcolumnDefs = [
    { width: "20px", targets: 0 },
    { width: "50px", targets: 5 },
    ];

    var table = $('#dataTable-SS').DataTable({
      "bProcessing": true,
      "language": {
        "decimal":        ",",
        "emptyTable":     "Data Tidak Tersedia...",
        "info":           "Tampil _START_ - _END_ dari _TOTAL_ Data",
        "infoEmpty":      "Tampil 0 to 0 of 0 Data",
        "infoFiltered":   "(filter dari _MAX_ total Data)",
        "infoPostFix":    "",
        "thousands":      ".",
        "lengthMenu":     "Tampil _MENU_ Data",
        "loadingRecords": "Loading...",
        "processing":     "Memperbarui Data",
        "searchPlaceholder": "Ketik Untuk Cari Data ...",
        "search":         "",
        "zeroRecords":    "Data Tidak Ditemukan",
        "paginate": {
          "first":      "Awal",
          "last":       "Akhir",
          "next":       "Lanjut",
          "previous":   "Kembali"
        }
      },
      "autoWidth": false,
      "ajax": {
        "url": apiPath + "&act=datatables",
        "type": "POST"
      },
      "bServerSide": true,
      "scrollX" : true,
      "scrollCollapse" : true,
      "aoColumns" : DtaoColumns,
      "columnDefs": DtcolumnDefs,
      "initComplete": function(settings, json) {
        // Tindakan Setelah Datatables Selesai Dijalankan
      }
    });
    
    var dataStatus = [{id:'1',text:'Kelas X'},{id:'2',text:'Kelas XI'},{id:'3',text:'Kelas XII'}];
    var dataMajor = <?= json_encode($db->select('major_id as id, major_name as text')->table('major')->getAll()) ?>;
    var dataTeacher = <?= json_encode($db->select('teacher_id as id, teacher_name as text')->table('teacher')->getAll()) ?>;

    $("#tambah-class_status").select2({
      data: dataStatus
    });

    $("#tambah-major_id").select2({
      data: dataMajor
    });

    $("#tambah-teacher_id").select2({
      data: dataTeacher
    });

    $("#edit-class_status").select2({
      data: dataStatus
    });

    $("#edit-major_id").select2({
      data: dataMajor
    });

    $("#edit-teacher_id").select2({
      data: dataTeacher
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-tambah").click(function(){

      var $form = $("#TambahForm");
      var data = $form.serialize();

      $.ajax({
        type: 'POST',
        url:apiPath + `&act=create`,
        data: data,
        success: function(data) {

          data = $.parseJSON(data);
          $('input[name="_token"]').val(data.message.csrf_token.newToken);

          if(data.error==false){
            $type='success';
            $message = 'Tambah Data Berhasil';

            $('.removeForm').trigger('click');
            getData();
          } else {
            $type='error';
            $message = 'Tambah Data Gagal';
          }

          $info = '';
          $.each(data.message.form, function(index, val) {
            $info += (index+1) + ". " + val + "<br>";
          });

          Swal.fire({
            type: $type,
            title: $message,
            html: $info,
            timer: 1500 * (data.message.form.length + 1)
          });
        }
      });
    });

    // Aksi Open Form Edit
    $(document).on('click','[data-edit]',function(){

      var $param = $(this);
      var editId = $param.data('edit');

      $.get(apiPath + `&act=read&${idKey}=`+editId, function(data) {
        data = $.parseJSON(data);
        $.each(data, function(index, val) {
          $('#edit-'+index).val(val).trigger('change');
        });
      });
      $('#edit-tab').show();
      $('a[href="#form-edit"]').trigger('click');
    });

    // Aksi Simpan Perubahan Data
    $(".tombol-edit").click(function(){

      var $form = $("#EditForm");
      var data = getFormData($form);
      var editId = data[idKey];
      var data = $form.serialize();

      $.ajax({
        type: 'POST',
        url:apiPath + `&act=update&${idKey}=`+editId,
        data: data,
        success: function(data) {

          data = $.parseJSON(data);

          $('input[name="_token"]').val(data.message.csrf_token.newToken);

          if(data.error==false){
            $type='success';
            $message = 'Ubah Data Berhasil';

            $('.removeForm').trigger('click');
            getData();
          } else {
            $type='error';
            $message = 'Ubah Data Gagal';
          }

          $info = '';
          $.each(data.message.form, function(index, val) {
            $info += (index+1) + ". " + val + "<br>";
          });

          Swal.fire({
            type: $type,
            title: $message,
            html: $info,
            timer: 1500 * (data.message.form.length + 1)
          });
        }
      });
    });

    // Aksi Hide Form Edit
    $(document).on('click','.removeForm',function(){
      $('#edit-tab').hide();
      $('a[href="#data"]').trigger('click');
      $('.form').trigger("reset");
      $('.form').trigger("change");
    });

    // Aksi Hapus
    $(document).on('click','[data-del]',function(){
      var $param = $(this);
      var deleteId = $param.data('del');

      Swal.fire({
        title: "Apakah Anda Yakin Menghapus Ingin Data?",
        text: "Jika Yakin, Silahkan Pilih Ya!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya, Hapus Sekarang',
        cancelButtonText: 'Tidak, Batalkan!',
        confirmButtonColor: '#d33',
        cancelButtonColor: '#333',
      }).then(isConfirm =>
      {
        if (isConfirm.value==true) {                       
          $.ajax({
            type: 'GET',    
            url: apiPath + "&act=delete",
            data: `${idKey}=`+deleteId,
            success: function(msg){
              Swal.fire("Terhapus!", msg, "success");
              getData();
            },
            error: function (request, kategori_sekolah, error) { 
              Swal.fire("Terjadi Kesalahan", request.responseText, "error");
            }
          });
        } else {
          Swal.fire("Dibatalkan", "Berhasil Membatalkan Penghapusan", "error");
        }
      })
    });

    function activityWatcher(){

      var secondsSinceLastActivity = 0;

      var maxInactivity = 10;

      setInterval(function(){
        secondsSinceLastActivity++;
        // if inactive >= 10 second
        if(secondsSinceLastActivity >= maxInactivity){
          // refresh data every 10 second
          (secondsSinceLastActivity%10)==0 ? getData() : '';
        }
      }, 1000);

      function activity(){
        secondsSinceLastActivity = 0;
      }

      var activityEvents = [
      'mousedown', 'mousemove', 'keydown',
      'scroll', 'touchstart'
      ];

      activityEvents.forEach(function(eventName) {
        document.addEventListener(eventName, activity, true);
      });
    }

    activityWatcher();
    
    function getData() {
      table.ajax.url(apiPath + "&act=datatables").load(null, true);
    }

    $('#reload-data').click(function(){
      getData();
    })
  }
</script>
