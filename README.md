# Attendance School System

* Jawaban Dikirim Ke Email dev.akarendra835@gmail.com *

Standart aplikasi yg dibuat akan menggunakan
- CRUD (Create Read Update Delete) Rest API dengan teknologi AJAX (Asynchronous Javascript and XML), dapat menggunakan JQuery ataupun ES5 (ECMAScript 5) keatas. (Wajib)
- Format Data Yg Diterima Dari Server (Backend menggunakan Bahasa Pemrograman PHP) dalam bentuk JSON (Wajib)
- Framework PHP yg digunakan tidak harus Laravel/Codeigniter, dapat menggunakan menggunakan framework lain seperti YII/Slim/Lumen/Zend/dll atau menggunakan framework pribadi yg berorientasi objek. (Penilaian Dilihat dari sisi Pattern Code dan Readability Code. Usahakan Coding Serapi Mungkin) (Wajib)
- Framework yg digunakan minimal menggunakan konsep MVC (Model View Controller) (Wajib)
- Database yg digunakan adalah MySQL/MariaDB/PerconaDB (Pilih Salah Satu) (Wajib)
- Menggunakan Datatables Serverside Processing lebih disukai. (Opsional)
- Menggunakan JWT(JSON Web Token) atau sejenisnya lebih disukai. (Opsional)

# Detail Standart Yg Dibutuhkan 
- Terdiri Dari 2 Table, users (untuk login) & client (data pelanggan) [struktur table minimal ada pada file soal_test.sql]
- Memiliki Fitur Login, Logout, Dan Kelola Module users dan client
- Menggunakan Library Jquery Select2 Untuk Dropdown Pilihan Tipe Pelanggan
- Menggunakan AJAX dengan response JSON sebagai standart penerima data pada sisi Frontend
- Pada Inputan No HP Client Menggunakan Format  ( XXXX-XXXX-XXXX ) menggunakan ES5 / JQuery
- Tidak Dapat Input No HP Yg Telah Ada Pada Database Pada Module Client
- Menggunakan Library JQuery Datatables pada sisi Frontend untuk table client dan users
- Menggunakan CSRF-TOKEN Lebih Disukai (Opsional)
- Menggunakan Datatables Serverside Processing lebih disukai. (Opsional)
- Menggunakan JWT(JSON Web Token) atau sejenisnya lebih disukai. (Opsional)

# Link Referensi Lainnya
# Codeigniter 
Datatables SSP :
- http://mfikri.com/artikel/Datatable-serverside-processing-menggunakan-codeigniter.html
- https://www.tutsntechs.com/tutorials/details/jquery-datatable-with-codeigniter-using-server-side-processing
- https://belajarphp.net/tutorial-codeigniter-datatables/
- https://www.youtube.com/watch?v=_Nug3YXqxI0

CRUD AJAX : 
- http://mfikri.com/en/blog/crud-codeigniter-ajax

Validation :
- https://www.guru99.com/codeigniter-form-validation.html

# Laravel

Datatables SSP: 
- https://belajarphp.net/tutorial-datatable-serverside-laravel/
- https://github.com/yajra/laravel-datatables
- https://adinata.id/laravel/server-side-datatables-menggunakan-yajra-1-pada-laravel-adminlte/

CRUD Ajax : 
- https://appdividend.com/2018/02/07/laravel-ajax-tutorial-example/

Validation :
- https://appdividend.com/2019/09/21/laravel-6-validation-example-validation-in-laravel-tutorial/