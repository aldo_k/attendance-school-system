<?php $period = $db->select('*')->table('period')->where('period_id',(int)@$_GET['id'])->get(); ?>
<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="datatables-tab" data-toggle="pill" href="#data" role="tab"
						aria-controls="data"><i class="icon icon-list-alt"></i> Semua Data Absensi</a>
					</li>
					<?php if($_SESSION['Level']==1){ ?>
						<li>
							<a class="nav-link " id="form-tambah-tab" data-toggle="pill" href="#form-tambah"
							role="tab" aria-controls="form-tambah"><i class="icon icon-notebook-list2"></i>Kelola Data Absensi</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</header>
	<!-- Start Tab Content -->
	<div class="container-fluid animatedParent animateOnce">
		<div class="tab-content my-3" id="v-pills-tabContent">
			<!-- Tab View Data Start -->
			<div class="tab-pane animated fadeInUpShort show active" id="data" role="tabpanel" aria-labelledby="v-pills-all-tab">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header white">
								<i class="icon-list-alt blue-text"></i>
								<strong> Data Absensi <?= $period->period_name ?></strong>
								<button type="button" class="btn btn-xs btn-primary r-5 float-right reload-data"><i class="icon-refresh"></i> Reload Data</button>
							</div>
							<div class="card-body">
								<div class="card-title">
									<div class="row form-inline">
										<div class="col-sm-12">
											<div class="form-group">
												<div class="col-sm-4">
													<select id="form-class_id" class="form-control r-0 light s-12" name="class_id" required="">
														<?php if($_SESSION['Level']==1){ ?>
															<option value="all">Semua Kelas </option>
														<?php } ?>
													</select>
												</div>
												<div class="col-sm-2">
													<select id="form-year_date" class="form-control r-0 light s-12" name="year_date" required="">
														<?php if($_SESSION['Level']==1){ ?>
															<option value="all">Semua Tahun </option>
														<?php } ?>
													</select>
												</div>
												<div class="col-sm-2">
													<select id="form-month_date" class="form-control r-0 light s-12" name="month_date" required="">
														<?php if($_SESSION['Level']==1){ ?>
															<option value="all">Semua Bulan </option>
														<?php } ?>
													</select>
												</div>
												<div class="col-sm-2">
													<button type="button" class="btn btn-sm btn-success r-5 reload-data"><i class="icon icon-search"></i>Filter Data Absen</button>
												</div>
												<div class="col-sm-2">
													<button type="button" class="btn btn-sm btn-danger r-5 print-pdf"><i class="icon icon-print2"></i>Cetak Laporan</button>
												</div>
											</div>
										</div>
									</div>
									<hr>
								</div>
								<table class="table table-bordered table-hover nowarp" id="dataTable-SS"></table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Tab View Data End -->
			<?php if($_SESSION['Level']!=3){ ?>
				<!-- Tab Tambah Data Start -->
				<div class="tab-pane animated fadeInUpShort" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
					<div class="row">
						<div class="col-md-4">
							<div class="card">
								<div class="card-header white">
									Kelola Data Absensi
								</div>
								<div class="card-content">
									<div class="card-body">
										<div class="card-text" id="notification-kelola"></div>
										<form id="kelolaAbsensi" class="form" action="javascript:void(0);" method="post">
											<?php echo csrf_field() ?>
											<div class="form-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label for="kelola-class_id">Kelas</label>
															<select id="kelola-class_id" class="form-control r-0 light s-12 select2-change" name="class_id" required="">
																<option value="">Silahkan Pilih Kelas</option>
															</select>
														</div>
													</div>
													<div class="col-md-12 text-center" id="tanggalAbsensi" style="display: none;">
														<label for="kelola-attendance_day">Silahkan Pilih Tanggal</label>
														<div class="form-group">
															<input type="text" class="date-time-picker form-control" id="kelola-attendance_day" name="tanggal" data-options='{"timepicker":false, "inline":true, "format":"Y-m-d"}'/>
														</div>
													</div>
													<hr>
												</div>
											</div>
											<div class="form-actions">
												<hr>
												<button class="btn btn-danger btn-sm mr-1 removeForm">
													<i class="icon-arrow_back mr-2"></i> Kembali
												</button>
												<button type="button" id="TombolSimpanAbsensi" class="btn btn-warning btn-sm mr-1 tombol-kelola" style="display:none" disabled="" title="Pilih Tanggal Terlebih Dahulu">
													<i class="icon-save mr-2"></i> Simpan Data Absensi
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="card">
								<div class="card-header white">
									<i class="icon-list-alt blue-text"></i>
									<strong> List View Data Absensi <span id="viewtglAbsensi"></span></strong>
								</div>
								<div class="card-body slimScroll" data-height="600">
									<div class="card-title text-center loadingList"> Silahkan Pilih Kelas & Tanggal Untuk Melakukan Absensi</div>
									<div class="col-sm-12 text-center loadingList">
										<div class="preloader-wrapper big active">
											<div class="spinner-layer spinner-blue">
												<div class="circle-clipper left">
													<div class="circle"></div>
												</div><div class="gap-patch">
													<div class="circle"></div>
												</div><div class="circle-clipper right">
													<div class="circle"></div>
												</div>
											</div>
											<div class="spinner-layer spinner-red">
												<div class="circle-clipper left">
													<div class="circle"></div>
												</div><div class="gap-patch">
													<div class="circle"></div>
												</div><div class="circle-clipper right">
													<div class="circle"></div>
												</div>
											</div>
											<div class="spinner-layer spinner-yellow">
												<div class="circle-clipper left">
													<div class="circle"></div>
												</div><div class="gap-patch">
													<div class="circle"></div>
												</div><div class="circle-clipper right">
													<div class="circle"></div>
												</div>
											</div>
											<div class="spinner-layer spinner-green">
												<div class="circle-clipper left">
													<div class="circle"></div>
												</div><div class="gap-patch">
													<div class="circle"></div>
												</div><div class="circle-clipper right">
													<div class="circle"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="table-responsive listData" style="display: none">
											<table class="table table-bordered table-hover nowarp" id="listSiswa">
												<!-- Table heading -->
												<thead>
													<tr class="no-b">
														<th width="70px">NIM</th>
														<th width="150px">Nama</th>
														<th>
															<input type="radio" class="radioAbsensiNya" id="allHadir" name="date[]" value="H"> Hadir
														</th>
														<th>
															<input type="radio" class="radioAbsensiNya" id="allTelat" name="date[]" value="T"> Telat
														</th>
														<th>
															<input type="radio" class="radioAbsensiNya" id="allIzin" name="date[]" value="I"> Izin
														</th>
														<th>
															<input type="radio" class="radioAbsensiNya" id="allSakit" name="date[]" value="S"> Sakit
														</th>
														<th>
															<input type="radio" class="radioAbsensiNya" id="allAlpha" name="date[]" value="A"> Alpha
														</th>
														<th>
															<input type="radio" class="radioAbsensiNya" id="allMinggat" name="date[]" value="M"> Minggat
														</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Tab Tambah Data End -->
			<?php } ?>
		</div>
	</div>
	<!-- End Tab Content -->
</div>
<script type="text/javascript">
	window.onload = function() {

		var idKey   = 'attendance_id';
		var apiPath = 'api.php?p=kehadiran';

		$('#menu-name').html('Data Absensi');
		$('.sidebar-menu').find('.menu-status').removeClass('active');
		$('.sidebar-menu').find('[data-menu="kehadiran"]').addClass('active');

    // Definisi Kolom Table
    DtaoColumns = [
    { "data": idKey, "title": "No", "name": idKey,"render": function ( data, type, row, meta ) {
    	return meta.row+meta.settings._iDisplayStart+1;
    }},
    { "data": "student_id", "title": "NIS", "name": "student_id" },
    { "data": "student_name", "title": "Nama Siswa", "name": "student_name" },
    { "data": "class_room", "title": "Kelas", "name": "class_room" },
    { "data": "attendance_month", "title": "Bulan", "name": "attendance_month", "render": function ( data, type, full ) {
    	return full['attendance_year'] + ', ' + dataMonth[(data-1)]['text'];
    }},
    { "data": "attendance_count_s", "title": "Sakit", "name": "attendance_count_s" },
    { "data": "attendance_count_i", "title": "Izin", "name": "attendance_count_i" },
    { "data": "attendance_count_a", "title": "Alpha", "name": "attendance_count_a" },
    { "data": "attendance_day01", "title": "01", "name": "attendance_day01", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day02", "title": "02", "name": "attendance_day02", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day03", "title": "03", "name": "attendance_day03", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day04", "title": "04", "name": "attendance_day04", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day05", "title": "05", "name": "attendance_day05", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day06", "title": "06", "name": "attendance_day06", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day07", "title": "07", "name": "attendance_day07", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day08", "title": "08", "name": "attendance_day08", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day09", "title": "09", "name": "attendance_day09", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day10", "title": "10", "name": "attendance_day10", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day11", "title": "11", "name": "attendance_day11", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day12", "title": "12", "name": "attendance_day12", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day13", "title": "13", "name": "attendance_day13", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day14", "title": "14", "name": "attendance_day14", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day15", "title": "15", "name": "attendance_day15", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day16", "title": "16", "name": "attendance_day16", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day17", "title": "17", "name": "attendance_day17", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day18", "title": "18", "name": "attendance_day18", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day19", "title": "19", "name": "attendance_day19", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day20", "title": "20", "name": "attendance_day20", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day21", "title": "21", "name": "attendance_day21", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day22", "title": "22", "name": "attendance_day22", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day23", "title": "23", "name": "attendance_day23", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day24", "title": "24", "name": "attendance_day24", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day25", "title": "25", "name": "attendance_day25", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day26", "title": "26", "name": "attendance_day26", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day27", "title": "27", "name": "attendance_day27", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day28", "title": "28", "name": "attendance_day28", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day29", "title": "29", "name": "attendance_day29", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day30", "title": "30", "name": "attendance_day30", "render": function(data){ 
    	return statusKeterangan(data);
    }},
    { "data": "attendance_day31", "title": "31", "name": "attendance_day31", "render": function(data){ 
    	return statusKeterangan(data);
    }}
    ];

    // Definisi Lebar Kolom
    DtcolumnDefs = [
    { width: "20px", targets: 0 },
    { width: "100px", targets: 1 },
    { width: "150px", targets: 2 },
    { width: "150px", targets: 3 },
    { width: "150px", targets: 4 },
    ];

    <?php 
    if($_SESSION['Level']==2){
    	$hari = date("N");
    	$class = $db->select('class.class_id as id, class.class_room as text, major_name as grup')
    	->table('class')
    	->join('`schedule`','`schedule`.class_id','class.class_id')
    	->join('major','major.major_id','class.major_id')
    	->where("`schedule`.teacher_id",$_SESSION['Reference'])
    	->where("`schedule`.schedule_day",$hari)
    	->getAll();
    } else if($_SESSION['Level']==1){
    	$class = $db->select('class_id as id, class_room as text, major_name as grup')->table('class')->join('major','major.major_id','class.major_id')->getAll();
    } else {
    	$class = $db->select('student.class_id as id, class_room as text, major_name as grup')->table('class')
    	->join('major','major.major_id','class.major_id')
    	->join('student','student.class_id','class.major_id')
    	->where("student_id",$_SESSION['Reference'])
    	->getAll();
    }
    $data = array_reduce($class, function(array $grup, $list){
    	$grup[$list->grup][] = ['id'=>$list->id,'text'=>$list->text];
    	return $grup;
    },[]);
    $classGroup = array();
    foreach ($data as $key => $value) {
    	$classGroup[] = [ 'text'=> "[ Jurusan $key ]", 'children'=> $value ];
    }

    $year = array();

    if($_SESSION['Level']==1){
    	for ($i = date('Y'); $i>=(date('Y')-10); $i--) {
    		$year[] = ['id'=>"$i",'text'=>"Tahun $i"];
    	}
    } else {
    	for ($i = date('Y'); $i>=(date('Y')); $i--) {
    		$year[] = ['id'=>"$i",'text'=>"Tahun $i"];
    	}
    }

    ?>

    var dataClass = <?= json_encode($classGroup); ?>;
    var dataYear 	= <?= json_encode($year) ?>;
    var dataMonth = [{id:'01',text:'Januari'},{id:'02',text:'Februari'},{id:'03',text:'Maret'},{id:'04',text:'April'},{id:'05',text:'Mei'},{id:'06',text:'Juni'},{id:'07',text:'Juli'},{id:'08',text:'Agustus'},{id:'09',text:'September'},{id:'10',text:'Oktober'},{id:'11',text:'November'},{id:'12',text:'Desember'}];

    var dataPeriod 	= <?= json_encode($listPeriod) ?>;

    $("#form-class_id").select2({
    	data: dataClass
    });

    $("#form-year_date").select2({
    	data: dataYear
    });

    $("#form-month_date").select2({
    	data: dataMonth
    });

    $("#kelola-class_id").select2({
    	data: dataClass
    });

    var Dtlanguage = {
    	"decimal":        ",",
    	"emptyTable":     "Data Tidak Tersedia...",
    	"info":           "Tampil _START_ - _END_ dari _TOTAL_ Data",
    	"infoEmpty":      "Tampil 0 to 0 of 0 Data",
    	"infoFiltered":   "(filter dari _MAX_ total Data)",
    	"infoPostFix":    "",
    	"thousands":      ".",
    	"lengthMenu":     "Tampil _MENU_ Data",
    	"loadingRecords": "Loading...",
    	"processing":     "Memperbarui Data",
    	"searchPlaceholder": "Ketik Untuk Cari Data ...",
    	"search":         "",
    	"zeroRecords":    "Data Tidak Ditemukan",
    	"paginate": {
    		"first":      "Awal",
    		"last":       "Akhir",
    		"next":       "Lanjut",
    		"previous":   "Kembali"
    	}
    };

    // Table Data
    var table = $('#dataTable-SS').DataTable({
    	<?php if($_SESSION['Level']!=1) { ?>
        "searching": false,
        "paging": false,
      <?php } ?>
    	"bProcessing": true,
    	"language": Dtlanguage,
    	// "autoWidth": false,
    	"ajax": {
    		"url": apiPath + "&act=datatables",
    		"type": "POST",
    		data: function ( d ) {
    			return $.extend( {}, d, {
    				"period_id": "<?= (int)$_GET['id'] ?>",
    				"class_id": $("#form-class_id").val(),
    				"attendance_year": $("#form-year_date").val(),
    				"attendance_month": $("#form-month_date").val(),
    			} );
    		}
    	},
    	"bServerSide": true,
    	"scrollX" : true,
    	"scrollCollapse" : true,
    	"aoColumns" : DtaoColumns,
    	"columnDefs": DtcolumnDefs,
    	"pageLength": 100,
    	"initComplete": function(settings, json) {
        // Tindakan Setelah Datatables Selesai Dijalankan
      }
    });

    // Table Data
    var listSiswa = $('#listSiswa').DataTable({
    	"autoWidth": false,
    	"searching": false,
    	"paging": false,
    	"info":     false,
    	"scrollX" : true,
    	"scrollCollapse" : true,
    	ajax : apiPath+'&act=listSiswa&class_id=0',
    	"aoColumns" : [
    	{ "data": "student_id", "name": "student_id" },
    	{ "data": "student_name", "name": "student_name", "orderable": false },
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"H","Hadir");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"T","Telat");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"I","Izin");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"S","Sakit");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"A","Alpha");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"M","Minggat");
    	}},
    	],
    	"columnDefs": [
    	{ "width": "25px", "targets": 0 },
    	{ "width": "450px", "targets": 1 },
    	{ "width": "100px", "targets": 2 },
    	{ "width": "100px", "targets": 3 },
    	{ "width": "100px", "targets": 4 },
    	{ "width": "100px", "targets": 5 },
    	{ "width": "100px", "targets": 6 },
    	{ "width": "100px", "targets": 7 },
    	]
    });

    $(document).on('click',".print-pdf", function(event) {
    	$ClassId 		= parseInt($('#form-class_id').val());
    	$YearDate 	= parseInt($('#form-year_date').val());
    	$MonthDate 	= parseInt($('#form-month_date').val());

    	if ($ClassId && $YearDate && $MonthDate) {
    		window.open(`print_kehadiran.php?period_id=<?= (int)$_GET['id'] ?>&class_id=${$ClassId}&year_date=${$YearDate}&month_date=${$MonthDate}`, '_blank');
    	} else {
    		$Text = "";
    		if (!$ClassId) {
    			if ($Text=="") {
    				$Text += "Kelas";
    			} else {
    				$Text += ", Kelas";
    			}
    		}

    		if (!$YearDate) {
    			if ($Text=="") {
    				$Text += "Tahun";
    			} else {
    				$Text += ", Tahun";
    			}
    		}

    		if (!$MonthDate) {
    			if ($Text=="") {
    				$Text += "Bulan";
    			} else {
    				$Text += ", Bulan";
    			}
    		}
    		Swal.fire("Maaf", `${$Text} Harus Dipilih Terlebih Dahulu`, "error");
    	}
    });


    $(document).on('change',"#kelolaAbsensi", function(event) {
    	var kelas = $('#kelola-class_id').val();
    	var tgl_k = $('#kelola-attendance_day').val();

    	if (kelas!="" && tgl_k=="") {
    		$('#tanggalAbsensi').show();
    		$('.loadingList').hide();
    		$('.listData').show();
    		listSiswa.ajax.url(apiPath+'&act=listSiswa&class_id='+kelas).load();

    		$('#TombolSimpanAbsensi').show();
    	} else if (kelas=="" && tgl_k=="") {
    		$('#tanggalAbsensi').hide();
    		$('.loadingList').show();
    		$('.listData').hide();

    		$('#TombolSimpanAbsensi').hide();
    	}

    	if (tgl_k!="") {
    		setTimeout(function(){ 
    			$.ajax({
    				url: apiPath+'&act=dataAbsensi',
    				type: 'POST',
    				dataType: 'json',
    				data: {period_id: '<?= (int)$_GET['id'] ?>',tanggal: tgl_k, class_id: kelas},
    			})
    			.done(function(data) {
    				$('.radioAbsensiNya').prop("checked", false);
    				if (data.data!=null) {
    					$.each(data.data, function(i, absensi) {
    						if (absensi.data!=null) {
    							dataNya = $.parseJSON(absensi.data);
    							$(`.absensi${dataNya.status}_${absensi.student_id}`).prop("checked", true);
    						}
    					});
    				}
    				$('#viewtglAbsensi').html("[ Tanggal : "+ tgl_k + " ]");
    				$('.radioAbsensiNya').removeAttr("disabled");
    				$('#TombolSimpanAbsensi').attr('class', 'btn btn-primary btn-sm mr-1 tombol-kelola');
    				$('#TombolSimpanAbsensi').removeAttr("disabled");
    				$('#TombolSimpanAbsensi').removeAttr("title");
    			});
    			
    		}, 500);
    	} else {
    		$('#viewtglAbsensi').html("");
    		$('.radioAbsensiNya').attr('disabled', 'disabled');
    		$('#TombolSimpanAbsensi').attr('class', 'btn btn-warning btn-sm mr-1 tombol-kelola');
    		$('#TombolSimpanAbsensi').attr('disabled', 'disabled');
    		$('#TombolSimpanAbsensi').attr('title', 'Pilih Tanggal Terlebih Dahulu');
    	}


    });

    // Aksi Simpan Perubahan Data
    $(".tombol-kelola").click(function(){
    	$dataAbsensi = $('.StatusAbsensiNya:checked').map(function(){
    		$InputName = $(this).attr("name");
    		$InputValue = $(this).attr("value");
    		return $InputName+"="+$InputValue;
    	}).get();
    	var $SubmitAbsensi = "";
    	$.each($dataAbsensi, function(index, val) {
    		$SubmitAbsensi += val + "&";
    	});

    	$ClassId = $('#kelola-class_id').val();
    	$Tanggal = $('#kelola-attendance_day').val();

    	if ($SubmitAbsensi!='') {
    		console.log();

    		$.ajax({
    			url: apiPath + `&act=update`,
    			type: 'POST',
    			dataType: 'json',
    			data: $SubmitAbsensi+"class_id="+$ClassId+"&attendance_day="+$Tanggal+"&period_id=<?= (int)$_GET['id'] ?>",
    		})
    		.done(function() {
    			setTimeout(
    				function(){
    					Swal.fire("Sukses!", "Data Absensi Berhasil Dikirim", "success");
    				}, 500);
    		});
    	}
    });

    // Aksi Hide Form Edit
    $(document).on('click','.removeForm',function(){
    	$('#edit-tab').hide();
    	$('a[href="#data"]').trigger('click');
    	$('.form').trigger("reset");
    	$('.form').trigger("change");
    	setTimeout(function(){ 
    		OnPageEdit = false;
    		table.ajax.url(apiPath + "&act=datatables").load(null, true);
    	}, 200);
    });

    // Aksi Hapus
    $(document).on('click','[data-del]',function(){
    	var $param = $(this);
    	var deleteId = $param.data('del');

    	Swal.fire({
    		title: "Apakah Anda Yakin Menghapus Ingin Data?",
    		text: "Jika Yakin, Silahkan Pilih Ya!",
    		type: 'warning',
    		showCancelButton: true,
    		confirmButtonText: 'Ya, Hapus Sekarang',
    		cancelButtonText: 'Tidak, Batalkan!',
    		confirmButtonColor: '#d33',
    		cancelButtonColor: '#333',
    	}).then(isConfirm =>
    	{
    		if (isConfirm.value==true) {                       
    			$.ajax({
    				type: 'GET',    
    				url: apiPath + "&act=delete",
    				data: `${idKey}=`+deleteId,
    				success: function(msg){
    					Swal.fire("Terhapus!", msg, "success");
    					getData();
    				},
    				error: function (request, kategori_sekolah, error) { 
    					Swal.fire("Terjadi Kesalahan", request.responseText, "error");
    				}
    			});
    		} else {
    			Swal.fire("Dibatalkan", "Berhasil Membatalkan Penghapusan", "error");
    		}
    	})
    });


    function activityWatcher(){

    	var secondsSinceLastActivity = 0;

    	var maxInactivity = 10;

    	setInterval(function(){
    		secondsSinceLastActivity++;
    		// if inactive >= 10 second
    		if(secondsSinceLastActivity >= maxInactivity){
    			// refresh data every 10 second
    			(secondsSinceLastActivity%10)==0 ? getData() : '';
    		}
    	}, 1000);

    	function activity(){
    		secondsSinceLastActivity = 0;
    	}

    	var activityEvents = [
    	'mousedown', 'mousemove', 'keydown',
    	'scroll', 'touchstart'
    	];

    	activityEvents.forEach(function(eventName) {
    		document.addEventListener(eventName, activity, true);
    	});
    }

    activityWatcher();

    function getData() {
    	table.ajax.url(apiPath + "&act=datatables").load(null, true);
    }

    function statusKeterangan(data){
    	$data = JSON.parse(data);
    	if ($data==null) {
    		return `<span class="badge badge-info text-white">-</span>`;
    	}
    	if ($data['status']=="H") {
    		return `<span class="badge badge-success">Hadir</span>`;
    	} else if($data['status']=="A"){
    		return `<span class="badge badge-danger">Alpha</span>`;
    	} else if($data['status']=="M"){
    		return `<span class="badge badge-dark">Minggat</span>`;
    	} else if($data['status']=="I"){
    		return `<span class="badge badge-warning">Izin</span>`;
    	} else if($data['status']=="S"){
    		return `<span class="badge badge-primary">Sakit</span>`;
    	} else if($data['status']=="T"){
    		return `<span class="badge badge-secondary">Telat</span>`;
    	} else if($data['status']=="L"){
    		return `<span class="badge badge-light">Libur</span>`;
    	}

    }

    function radioAbsensi(data, value, keterangan){
    	return `<label><input type="radio" class="radioAbsensiNya StatusAbsensiNya status_${value} absensi_${data} absensi${value}_${data}" name="status[${data}]" value="${value}" disabled> ${value} </label>`;
    }

    $("#allHadir").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_H").prop("checked", true);
    	$("#allHadir").prop("checked", true);
    });

    $("#allAlpha").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_A").prop("checked", true);
    	$("#allAlpha").prop("checked", true);
    });

    $("#allMinggat").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_M").prop("checked", true);
    	$("#allMinggat").prop("checked", true);
    });

    $("#allIzin").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_I").prop("checked", true);
    	$("#allIzin").prop("checked", true);
    });

    $("#allSakit").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_S").prop("checked", true);
    	$("#allSakit").prop("checked", true);
    });

    $("#allTelat").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_T").prop("checked", true);
    	$("#allTelat").prop("checked", true);
    });

    $('.reload-data').click(function(){
    	getData();
    })
  }
</script>