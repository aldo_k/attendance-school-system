<?php 
date_default_timezone_set('Asia/Jakarta');
require(__DIR__ . '/modules/config.php'); 

if (@$_SESSION['IsLogin']!=true) {
	echo "<script>location.href='index.php'</script>";
	exit();
}

$IndexBulan = array (
	1 =>   'JANUARI',
	'FEBRUARI',
	'MARET',
	'APRIL',
	'MEI',
	'JUNO',
	'JULI',
	'AGUSTUS',
	'SEPTEMBER',
	'OKTOBER',
	'NOVEMBER',
	'DESEMBER'
);
$attendance_month = sprintf('%02s', (int)@$_GET['month_date']);
$attendance = $db->select('attendance.*, student.student_name as student_name')->table('attendance')
->join('student','student.student_id','attendance.student_id')
->where('period_id',(int)@$_GET['period_id'])
->where('attendance.class_id',(int)@$_GET['class_id'])
->where('attendance_year',(int)@$_GET['year_date'])
->where('attendance_month',$attendance_month)
->getAll();

$period 		= $db->select('*')->table('period')->where('period_id',(int)@$_GET['period_id'])->get();
$DataClass 	= $db->select('*')->table('class')->join('major','class.major_id','major.major_id')->where('class_id',(int)@$_GET['class_id'])->get();

$Kelas 		= $DataClass->class_room;
$Jurusan 	= $DataClass->major_name;
$Tahun 		= (int)@$_GET['year_date'];
$Bulan 		= @$IndexBulan[(int)@$_GET['month_date']];

ob_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Laporan PDF</title>
	<link rel="stylesheet" type="text/css" href="assets/css/laporan.css" />
	<style>span{font-family: dejavusans;}</style>
</head>
<body>
	<div id="isi">
		<div id="title-kopcetak" style="line-height: 1.3;font-size: 11pt;">
			LAPORAN ABSENSI PERIODE <?= $period->period_name ?>
			<br>
			SMK NEGERI 5 PALEMBANG
		</div>
		<hr>
		<br>
		<div>
			<div style="display:inline; width:600px; padding:7px;font-size:10pt;font-weight: bold;">Jurusan &nbsp;: <?= $Jurusan ?><br><br> Kelas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?= $Kelas ?></div>
		</div>
		<table width="100%" border="0.5" cellpadding="0" cellspacing="0" align="left" style="font-size: 9px">
			<thead>
				<tr>
					<th height="15" width="15" align="center" valign="middle" style="padding:2.5px" rowspan="2">No</th>
					<th height="15" width="75" align="center" valign="middle" style="padding:2.5px" rowspan="2">NIS</th>
					<th height="15" width="150" align="center" valign="middle" style="padding:2.5px" rowspan="2">NAMA LENGKAP</th>
					<th height="15" align="center" valign="middle" style="padding:2.5px" colspan="31">ABSENSI - <?= $Bulan . " " . $Tahun ?></th>
					<th height="15" align="center" valign="middle" style="padding:2.5px" colspan="3">TOTAL</th>
				</tr>
				<tr>
					<?php for ($i=1; $i < 32 ; $i++) { ?>
						<th height="15" width="10.5" align="center" valign="middle" style="padding:2.5px"><?= sprintf("%02s", $i) ?></th>
					<?php } ?>
					<th height="15" width="20" align="center" valign="middle" style="padding:2.5px">Sakit</th>
					<th height="15" width="20" align="center" valign="middle" style="padding:2.5px">Izin</th>
					<th height="15" width="20" align="center" valign="middle" style="padding:2.5px">Alpha</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				$total_s = 0;
				$total_i = 0;
				$total_a = 0;
				for ($i=1; $i < 32 ; $i++) { 
					$totalH[$i]=0;
				}

				foreach ($attendance as $key => $value) {
					?>
					<tr>
						<td height="15" width="15" align="center" valign="middle" style="padding:2.5px"><?= $key+1 ?></td>
						<td height="15" width="75" align="center" valign="middle" style="padding:2.5px"><?= $value->student_id ?></td>
						<td height="15" width="150" align="center" valign="middle" style="padding:2.5px"><?= $value->student_name ?></td>
						<?php 
						for ($i=1; $i < 32 ; $i++) { 
							$absensi = $value->{'attendance_day' . sprintf("%02s", $i)};
							if ($absensi==null) {
								$statusAbsensi = "-";
							} else {
								$absensi = json_decode($absensi);
								$statusAbsensi = $absensi->status;
								if ($statusAbsensi=="H" || $statusAbsensi=="T") {
									$totalH[$i] += 1;
								}
							}
							if ($statusAbsensi=="-") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px\">$statusAbsensi</td>";
							} else if ($statusAbsensi=="H") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px; background:#338c04;\">$statusAbsensi</td>";
							} else if ($statusAbsensi=="T") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px; background:#6c757d;\">$statusAbsensi</td>";
							} else if ($statusAbsensi=="S") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px; background:#78BCEE;\">$statusAbsensi</td>";
							} else if ($statusAbsensi=="I") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px; background:#FCCE54;\">$statusAbsensi</td>";
							} else if ($statusAbsensi=="A") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px; background:#ED5564;\">$statusAbsensi</td>";
							} else if ($statusAbsensi=="M") {
								echo "<td height=\"15\" width=\"10.5\" align=\"center\" valign=\"middle\" style=\"padding:2.5px; background:#971B38;\">$statusAbsensi</td>";
							}
						}
						$total_s += $value->attendance_count_s;
						$total_i += $value->attendance_count_i;
						$total_a += $value->attendance_count_a;
						?>
						<td height="15" width="20" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $value->attendance_count_s ?></b></td>
						<td height="15" width="20" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $value->attendance_count_i ?></b></td>
						<td height="15" width="20" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $value->attendance_count_a ?></b></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<th height="15" align="right" valign="middle" style="padding:2.5px; font-size:10px;" colspan="3">TOTAL KEHADIRAN HARIAN</th>
					<?php 
					for ($i=1; $i < 32 ; $i++) { 
						?>
						<th height="15" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $totalH[$i] ?></b></th>
						<?php 
					} 
					?>
					<th height="15" align="right" valign="middle" style="padding:2.5px; font-size:10px;" colspan="3">----------------------------</th>
				</tr>
				<tr>
					<th height="15" align="right" valign="middle" style="padding:2.5px; font-size:10px;" colspan="34">TOTAL SELURUH</th>
					<th height="15" width="20" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $total_s ?></b></th>
					<th height="15" width="20" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $total_i ?></b></th>
					<th height="15" width="20" align="center" valign="middle" style="padding:2.5px; font-size:10px;"><b><?= $total_a ?></b></th>
				</tr>
			</tfoot>
		</table>
	</div>
	<br>
	<div style="width: 100%">
		<div style="margin-left: 25px;line-height:1.5;" id="footer-tanggal">

		</div>
		<div style="margin-left: 25px;line-height:1.5;" id="footer-tanggal">

		</div>
		<div style="margin:-170px 0 0 775px;line-height:1.5;" id="footer-tanggal">
			Diketahui Oleh, <br> Kepala Sekolah SMK NEGERI 5 PALEMBANG
		</div>
		<div style="margin:90px 0 0 775px;line-height:1.5;" id="footer-tanggal">
			<b><u>.....................................................................</u></b><br> NIP : ............................................................
		</div>
	</div>
</body>
</html>
<?php
use Spipu\Html2Pdf\Html2Pdf;

$filename = "Laporan Absensi Kelas $Kelas - [ $Bulan $Tahun ] .pdf";
//==========================================================================================================
$content = ob_get_clean();
$content = '<page style="font-family: freeserif">' . ($content) . '</page>';
try
{
	$html2pdf = new HTML2PDF('L', 'A4', 'en', true, 'UTF-8', array(8, 10, 8, 10));
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->setDefaultFont('arial');
	$html2pdf->AddFont('dejavusans');
	$html2pdf->writeHTML($content);
	$html2pdf->Output($filename);
} catch (HTML2PDF_exception $e) {echo $e;}