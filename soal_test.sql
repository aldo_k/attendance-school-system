-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2020 at 07:27 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `impro`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_client`
--

CREATE TABLE `master_client` (
  `master_client_id` int(11) NOT NULL,
  `master_client_name` varchar(255) NOT NULL COMMENT 'Nama Client',
  `master_brand_id` varchar(255) NOT NULL COMMENT 'Brand',
  `master_client_address` text NOT NULL COMMENT 'Alamat',
  `master_city_id` varchar(255) NOT NULL COMMENT 'Kota',
  `master_client_phone_office` varchar(16) DEFAULT NULL COMMENT 'No Kantor',
  `master_client_phone_mobile` varchar(16) NOT NULL COMMENT 'No HP',
  `master_client_pic` varchar(255) DEFAULT NULL COMMENT 'Penanggungjawab',
  `master_client_section` varchar(128) DEFAULT NULL COMMENT 'Bagian',
  `master_client_email` varchar(255) DEFAULT NULL COMMENT 'Email Client',
  `master_client_date_of_reg` date NOT NULL COMMENT 'Tanggal Daftar',
  `master_client_type` enum('Retail','Coorporate') NOT NULL DEFAULT 'Retail' COMMENT 'Tipe Pelanggan',
  `master_client_status` enum('Active','Non Active') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('active','nonactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'nonactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_client`
--
ALTER TABLE `master_client`
  ADD PRIMARY KEY (`master_client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_client`
--
ALTER TABLE `master_client`
  MODIFY `master_client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
