-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 15, 2019 at 08:12 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `attendance_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `attendance_year` varchar(4) NOT NULL,
  `attendance_month` varchar(2) NOT NULL,
  `student_id` varchar(6) NOT NULL,
  `attendance_count_s` tinyint(2) NOT NULL,
  `attendance_count_i` tinyint(2) NOT NULL,
  `attendance_count_a` tinyint(2) NOT NULL,
  `attendance_day01` text DEFAULT NULL,
  `attendance_day02` text DEFAULT NULL,
  `attendance_day03` text DEFAULT NULL,
  `attendance_day04` text DEFAULT NULL,
  `attendance_day05` text DEFAULT NULL,
  `attendance_day06` text DEFAULT NULL,
  `attendance_day07` text DEFAULT NULL,
  `attendance_day08` text DEFAULT NULL,
  `attendance_day09` text DEFAULT NULL,
  `attendance_day10` text DEFAULT NULL,
  `attendance_day11` text DEFAULT NULL,
  `attendance_day12` text DEFAULT NULL,
  `attendance_day13` text DEFAULT NULL,
  `attendance_day14` text DEFAULT NULL,
  `attendance_day15` text DEFAULT NULL,
  `attendance_day16` text DEFAULT NULL,
  `attendance_day17` text DEFAULT NULL,
  `attendance_day18` text DEFAULT NULL,
  `attendance_day19` text DEFAULT NULL,
  `attendance_day20` text DEFAULT NULL,
  `attendance_day21` text DEFAULT NULL,
  `attendance_day22` text DEFAULT NULL,
  `attendance_day23` text DEFAULT NULL,
  `attendance_day24` text DEFAULT NULL,
  `attendance_day25` text DEFAULT NULL,
  `attendance_day26` text DEFAULT NULL,
  `attendance_day27` text DEFAULT NULL,
  `attendance_day28` text DEFAULT NULL,
  `attendance_day29` text DEFAULT NULL,
  `attendance_day30` text DEFAULT NULL,
  `attendance_day31` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`attendance_id`, `period_id`, `class_id`, `attendance_year`, `attendance_month`, `student_id`, `attendance_count_s`, `attendance_count_i`, `attendance_count_a`, `attendance_day01`, `attendance_day02`, `attendance_day03`, `attendance_day04`, `attendance_day05`, `attendance_day06`, `attendance_day07`, `attendance_day08`, `attendance_day09`, `attendance_day10`, `attendance_day11`, `attendance_day12`, `attendance_day13`, `attendance_day14`, `attendance_day15`, `attendance_day16`, `attendance_day17`, `attendance_day18`, `attendance_day19`, `attendance_day20`, `attendance_day21`, `attendance_day22`, `attendance_day23`, `attendance_day24`, `attendance_day25`, `attendance_day26`, `attendance_day27`, `attendance_day28`, `attendance_day29`, `attendance_day30`, `attendance_day31`) VALUES
(13, 1, 1, '2019', '12', '33', 1, 1, 2, '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:03:52\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:01\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"I\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:08\",\"status\":\"I\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:16\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:22\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:27\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:31\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:36\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:41\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:46\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:52\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 03:31:26\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:58\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:06\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:22\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:15\",\"status\":\"H\"}],\"guru\":[]}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 1, 1, '2019', '12', '111', 1, 0, 1, '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:03:52\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:01\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:08\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:16\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:22\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:27\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:31\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:36\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:41\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:46\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:52\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 03:31:26\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:58\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:06\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:22\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:15\",\"status\":\"H\"}],\"guru\":[]}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, 1, '2019', '12', '222', 1, 0, 2, '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:03:52\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:01\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:08\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:16\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:22\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:27\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:31\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:36\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:41\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:46\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:52\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"M\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 03:31:27\",\"status\":\"M\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:58\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:06\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:22\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:15\",\"status\":\"H\"}],\"guru\":[]}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 1, 1, '2019', '12', '333', 1, 0, 1, '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:03:52\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:01\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:08\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:16\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:22\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:27\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:31\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:36\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:41\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:46\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:52\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 03:31:27\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:58\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:06\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:22\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:15\",\"status\":\"H\"}],\"guru\":[]}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 1, 1, '2019', '12', '444', 1, 1, 1, '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:03:52\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:01\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:08\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:16\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:22\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:27\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:31\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:36\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:41\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:46\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:52\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"I\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 03:31:27\",\"status\":\"I\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:58\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:06\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:22\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:15\",\"status\":\"H\"}],\"guru\":[]}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 1, 1, '2019', '12', '123456', 2, 1, 1, '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:03:52\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:01\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"I\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:08\",\"status\":\"I\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:16\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:22\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:27\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:31\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:36\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:41\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:46\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"T\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:52\",\"status\":\"T\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 03:31:27\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:04:58\",\"status\":\"H\"}],\"guru\":[]}}', '{\"status\":\"A\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:06\",\"status\":\"A\"}],\"guru\":[]}}', '{\"status\":\"S\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:22\",\"status\":\"S\"}],\"guru\":[]}}', '{\"status\":\"H\",\"log\":{\"admin\":[{\"admin_id\":\"1\",\"datetime\":\"2019-12-12 05:06:15\",\"status\":\"H\"}],\"guru\":[]}}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `class_id` int(11) NOT NULL,
  `class_room` varchar(128) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `major_id` int(11) NOT NULL,
  `class_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_id`, `class_room`, `teacher_id`, `major_id`, `class_status`) VALUES
(1, 'X TKJ A', 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `major`
--

CREATE TABLE `major` (
  `major_id` int(11) NOT NULL,
  `major_name` varchar(128) NOT NULL COMMENT 'Nama Jurusan',
  `major_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `major`
--

INSERT INTO `major` (`major_id`, `major_name`, `major_status`) VALUES
(1, 'Teknik Komputer Jaringan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `period`
--

CREATE TABLE `period` (
  `period_id` int(11) NOT NULL,
  `period_name` varchar(128) NOT NULL,
  `period_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `period`
--

INSERT INTO `period` (`period_id`, `period_name`, `period_status`) VALUES
(1, 'Periode 2018/2019', 1),
(2, 'Periode 2019/2020', 0);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `schedule_id` int(11) NOT NULL,
  `schedule_code` varchar(128) NOT NULL,
  `schedule_name` varchar(128) NOT NULL,
  `schedule_day` tinyint(1) NOT NULL,
  `schedule_starttime` varchar(5) NOT NULL,
  `schedule_endtime` varchar(5) NOT NULL,
  `class_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`schedule_id`, `schedule_code`, `schedule_name`, `schedule_day`, `schedule_starttime`, `schedule_endtime`, `class_id`, `teacher_id`, `period_id`) VALUES
(1, 'TKJ RPL XA', 'Rekayasa Perangkat Lunak', 5, '08:00', '09:00', 1, 6, 1),
(2, 'X.A TKJ JDXA', 'Jaringan Dasar 1', 1, '09:30', '11:00', 1, 6, 1),
(3, '1234', 'Informatika', 4, '09:22', '11:22', 1, 6, 1),
(4, '123123', 'Entah Apo Dio', 3, '12:00', '13:00', 1, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `student_id` varchar(6) NOT NULL,
  `student_name` varchar(128) NOT NULL,
  `student_gender` tinyint(1) NOT NULL,
  `student_address` text NOT NULL,
  `class_id` int(11) NOT NULL,
  `student_phone1` varchar(18) NOT NULL,
  `student_phone2` varchar(18) NOT NULL,
  `student_phone3` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`student_id`, `student_name`, `student_gender`, `student_address`, `class_id`, `student_phone1`, `student_phone2`, `student_phone3`) VALUES
('111', '111', 1, '111', 1, '111', '111', '111'),
('123456', 'Azka', 1, '123', 1, '123', '123', '123'),
('222', '222', 1, '222', 1, '222', '222', '222'),
('33', 'qqq', 1, '33', 1, '33', '33', '333'),
('333', '333', 0, '333', 1, '333', '333', '333'),
('444', '444', 1, '444', 1, '444', '444', '444');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `teacher_id` int(11) NOT NULL,
  `teacher_name` varchar(128) NOT NULL,
  `teacher_email` varchar(255) NOT NULL,
  `teacher_phone` varchar(18) NOT NULL,
  `teacher_details` text NOT NULL,
  `teacher_gender` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`teacher_id`, `teacher_name`, `teacher_email`, `teacher_phone`, `teacher_details`, `teacher_gender`) VALUES
(6, 'Imron', '123', '123', '123', 1),
(7, 'Husni', 'husni@gmail.com', '111', 'Guru Matematika', 1),
(8, 'amante', '111', '111', '111', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `fullname` varchar(128) NOT NULL,
  `username` varchar(18) NOT NULL,
  `password` varchar(128) NOT NULL,
  `users_type` tinyint(1) NOT NULL,
  `ref_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `fullname`, `username`, `password`, `users_type`, `ref_id`) VALUES
(1, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '0'),
(139, 'Imron', '123123', '202cb962ac59075b964b07152d234b70', 2, '6'),
(140, 'Azka', '123', '202cb962ac59075b964b07152d234b70', 3, '123456'),
(141, '111', '111', '698d51a19d8a121ce581499d7b701668', 3, '111'),
(142, '222', '222', 'bcbe3365e6ac95ea2c0343a2395834dd', 3, '222'),
(143, '333', '333', '310dcbbf4cce62f762a2aaa148d556bd', 3, '333'),
(144, '444', '444', '550a141f12de6341fba65b0ad0433500', 3, '444'),
(145, 'Husni', 'husni', 'e10adc3949ba59abbe56e057f20f883e', 2, '7'),
(146, 'amante', '1111', 'b59c67bf196a4758191e42f76670ceba', 2, '8'),
(147, 'qqq', '33333', 'b7bc2a2f5bb6d521e64c8974c143e9a0', 3, '33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `major`
--
ALTER TABLE `major`
  ADD PRIMARY KEY (`major_id`);

--
-- Indexes for table `period`
--
ALTER TABLE `period`
  ADD PRIMARY KEY (`period_id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`teacher_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `major`
--
ALTER TABLE `major`
  MODIFY `major_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `period`
--
ALTER TABLE `period`
  MODIFY `period_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `teacher_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
