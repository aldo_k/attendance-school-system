<?php $period = $db->select('*')->table('period')->where('period_id',(int)@$_GET['id'])->get(); ?>
<div class="page bg-light height-full">
	<header class="blue accent-3 relative">
		<div class="container-fluid text-white">
			<div class="row justify-content-between">
				<ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
					<li>
						<a class="nav-link active" id="form-tambah-tab" data-toggle="pill" href="#form-tambah"
						role="tab" aria-controls="form-tambah"><i class="icon icon-notebook-list2"></i>Kelola Data Absensi</a>
					</li>
				</ul>
			</div>
		</div>
	</header>
	<!-- Start Tab Content -->
	<div class="container-fluid animatedParent animateOnce">
		<div class="tab-content my-3" id="v-pills-tabContent">
			<!-- Tab Tambah Data Start -->
			<div class="tab-pane animated fadeInUpShort show active" id="form-tambah" role="tabpanel" aria-labelledby="v-pills-all-tab">
				<div class="row">
					<div class="col-md-4">
						<div class="card">
							<div class="card-header white">
								Kelola Data Absensi
							</div>
							<div class="card-content">
								<div class="card-body">
									<div class="card-text" id="notification-kelola"></div>
									<form id="kelolaAbsensi" class="form" action="javascript:void(0);" method="post">
										<?php echo csrf_field() ?>
										<div class="form-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<label for="kelola-class_id">Kelas</label>
														<select id="kelola-class_id" class="form-control r-0 light s-12 select2-change" name="class_id" required="">
														</select>
													</div>
												</div>
												<div class="col-md-12 text-center" id="tanggalAbsensi" style="display: none;">
													<label for="kelola-attendance_day">Tanggal Hari Ini</label>
													<div class="form-group">
														<input type="text" class="date-time-picker form-control" id="kelola-attendance_day" name="tanggal" data-options='{"timepicker":false, "inline":true, "format":"Y-m-d"}' />
													</div>
												</div>
												<hr>
											</div>
										</div>
										<div class="form-actions">
											<hr>
											<button class="btn btn-danger btn-sm mr-1 removeForm">
												<i class="icon-arrow_back mr-2"></i> Kembali
											</button>
											<button type="button" id="TombolSimpanAbsensi" class="btn btn-warning btn-sm mr-1 tombol-kelola" style="display:none" disabled="" title="Pilih Tanggal Terlebih Dahulu">
												<i class="icon-save mr-2"></i> Simpan Data Absensi
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="card">
							<div class="card-header white">
								<i class="icon-list-alt blue-text"></i>
								<strong> List View Data Absensi <span id="viewtglAbsensi"></span></strong>
							</div>
							<div class="card-body slimScroll" data-height="600">
								<div class="card-title text-center loadingList"> Silahkan Pilih Kelas & Tanggal Untuk Melakukan Absensi</div>
								<div class="col-sm-12 text-center loadingList">
									<div class="preloader-wrapper big active">
										<div class="spinner-layer spinner-blue">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div><div class="gap-patch">
												<div class="circle"></div>
											</div><div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
										<div class="spinner-layer spinner-red">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div><div class="gap-patch">
												<div class="circle"></div>
											</div><div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
										<div class="spinner-layer spinner-yellow">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div><div class="gap-patch">
												<div class="circle"></div>
											</div><div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
										<div class="spinner-layer spinner-green">
											<div class="circle-clipper left">
												<div class="circle"></div>
											</div><div class="gap-patch">
												<div class="circle"></div>
											</div><div class="circle-clipper right">
												<div class="circle"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="table-responsive listData" style="display: none">
										<table class="table table-bordered table-hover nowarp" id="listSiswa">
											<!-- Table heading -->
											<thead>
												<tr class="no-b">
													<th width="70px">NIM</th>
													<th width="150px">Nama</th>
													<th>
														<input type="radio" class="radioAbsensiNya" id="allHadir" name="date[]" value="H"> Hadir
													</th>
													<th>
														<input type="radio" class="radioAbsensiNya" id="allTelat" name="date[]" value="T"> Telat
													</th>
													<th>
														<input type="radio" class="radioAbsensiNya" id="allIzin" name="date[]" value="I"> Izin
													</th>
													<th>
														<input type="radio" class="radioAbsensiNya" id="allSakit" name="date[]" value="S"> Sakit
													</th>
													<th>
														<input type="radio" class="radioAbsensiNya" id="allAlpha" name="date[]" value="A"> Alpha
													</th>
													<th>
														<input type="radio" class="radioAbsensiNya" id="allMinggat" name="date[]" value="M"> Minggat
													</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Tab Tambah Data End -->
		</div>
	</div>
	<!-- End Tab Content -->
</div>
<script type="text/javascript">
	window.onload = function() {

		var idKey   = 'attendance_id';
		var apiPath = 'api.php?p=kehadiran';

		$('#menu-name').html('Data Absensi');
		$('.sidebar-menu').find('.menu-status').removeClass('active');
		$('.sidebar-menu').find('[data-menu="kehadiran"]').addClass('active');

		<?php 
		if($_SESSION['Level']==2){
			$hari = date("N");
			$class = $db->select('class.class_id as id, class.class_room as text, major_name as grup')
			->table('class')
			->join('`schedule`','`schedule`.class_id','class.class_id')
			->join('major','major.major_id','class.major_id')
			->where("`schedule`.teacher_id",$_SESSION['Reference'])
			->where("`schedule`.schedule_day",$hari)
			->getAll();
		} else if($_SESSION['Level']==1){
			$class = $db->select('class_id as id, class_room as text, major_name as grup')->table('class')->join('major','major.major_id','class.major_id')->getAll();
		} else {
			$class = $db->select('student.class_id as id, class_room as text, major_name as grup')->table('class')
			->join('major','major.major_id','class.major_id')
			->join('student','student.class_id','class.major_id')
			->where("student_id",$_SESSION['Reference'])
			->getAll();
		}
		$data = array_reduce($class, function(array $grup, $list){
			$grup[$list->grup][] = ['id'=>$list->id,'text'=>$list->text];
			return $grup;
		},[]);
		$classGroup = array();
		foreach ($data as $key => $value) {
			$classGroup[] = [ 'text'=> "[ Jurusan $key ]", 'children'=> $value ];
		}

		?>

		var dataClass = <?= json_encode($classGroup); ?>;
		var dataPeriod 	= <?= json_encode($listPeriod) ?>;

		$("#kelola-class_id").select2({
			data: dataClass
		});

		setTimeout(function(){
			$("#kelola-class_id").val('<?= (int)@$_GET['class_id'] ?>').trigger('change');
			$("#kelola-attendance_day").val('<?= date('Y-m-d') ?>').trigger('change');
		}, 50);

		setTimeout(function(){
			$("#kelolaAbsensi").trigger('change');
			$("#kelolaAbsensi :input").prop('disabled', true).trigger('click');
			$("#TombolSimpanAbsensi, .removeForm").prop('disabled', false);
		}, 50);

		setTimeout(function(){
			$('#TombolSimpanAbsensi').attr('class', 'btn btn-primary btn-sm mr-1 tombol-kelola');
		}, 50);

    // Table Data
    var listSiswa = $('#listSiswa').DataTable({
    	"autoWidth": false,
    	"searching": false,
    	"paging": false,
    	"info":     false,
    	"scrollX" : true,
    	"scrollCollapse" : true,
    	ajax : apiPath+'&act=listSiswa&class_id=0',
    	"aoColumns" : [
    	{ "data": "student_id", "name": "student_id" },
    	{ "data": "student_name", "name": "student_name", "orderable": false },
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"H","Hadir");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"T","Telat");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"I","Izin");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"S","Sakit");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"A","Alpha");
    	}},
    	{ "data": "student_id", "name": "student_id", "orderable": false, "render": function(data){ 
    		return radioAbsensi(data,"M","Minggat");
    	}},
    	],
    	"columnDefs": [
    	{ "width": "25px", "targets": 0 },
    	{ "width": "450px", "targets": 1 },
    	{ "width": "100px", "targets": 2 },
    	{ "width": "100px", "targets": 3 },
    	{ "width": "100px", "targets": 4 },
    	{ "width": "100px", "targets": 5 },
    	{ "width": "100px", "targets": 6 },
    	{ "width": "100px", "targets": 7 },
    	]
    });

    $(document).on('change',"#kelolaAbsensi", function(event) {
    	var kelas = $('#kelola-class_id').val();
    	var tgl_k = $('#kelola-attendance_day').val();

    	if (kelas!="" && tgl_k=="") {
    		$('#tanggalAbsensi').show();
    		$('.loadingList').hide();
    		$('.listData').show();
    		listSiswa.ajax.url(apiPath+'&act=listSiswa&class_id='+kelas).load();

    		$('#TombolSimpanAbsensi').show();
    	} else if (kelas=="" && tgl_k=="") {
    		$('#tanggalAbsensi').hide();
    		$('.loadingList').show();
    		$('.listData').hide();

    		$('#TombolSimpanAbsensi').hide();
    	}

    	if (tgl_k!="") {
    		setTimeout(function(){ 
    			$.ajax({
    				url: apiPath+'&act=dataAbsensi',
    				type: 'POST',
    				dataType: 'json',
    				data: {period_id: '<?= (int)$_GET['id'] ?>',tanggal: tgl_k, class_id: kelas},
    			})
    			.done(function(data) {
    				$('.radioAbsensiNya').prop("checked", false);
    				if (data.data!=null) {
    					$.each(data.data, function(i, absensi) {
    						if (absensi.data!=null) {
    							dataNya = $.parseJSON(absensi.data);
    							$(`.absensi${dataNya.status}_${absensi.student_id}`).prop("checked", true);
    						}
    					});
    				}
    				$('#viewtglAbsensi').html("[ Tanggal : "+ tgl_k + " ]");
    				$('.radioAbsensiNya').removeAttr("disabled");
    				$('#TombolSimpanAbsensi').attr('class', 'btn btn-primary btn-sm mr-1');
    				$('#TombolSimpanAbsensi').removeAttr("disabled");
    				$('#TombolSimpanAbsensi').removeAttr("title");
    			});
    			
    		}, 500);
    	} else {
    		$('#viewtglAbsensi').html("");
    		$('.radioAbsensiNya').attr('disabled', 'disabled');
    		$('#TombolSimpanAbsensi').attr('class', 'btn btn-warning btn-sm mr-1');
    		$('#TombolSimpanAbsensi').attr('disabled', 'disabled');
    		$('#TombolSimpanAbsensi').attr('title', 'Pilih Tanggal Terlebih Dahulu');
    	}


    });

    // Aksi Simpan Perubahan Data
    $(".tombol-kelola").click(function(){
    	$dataAbsensi = $('.StatusAbsensiNya:checked').map(function(){
    		$InputName = $(this).attr("name");
    		$InputValue = $(this).attr("value");
    		return $InputName+"="+$InputValue;
    	}).get();
    	var $SubmitAbsensi = "";
    	$.each($dataAbsensi, function(index, val) {
    		$SubmitAbsensi += val + "&";
    	});

    	$ClassId = $('#kelola-class_id').val();
    	$Tanggal = $('#kelola-attendance_day').val();
    	if ($SubmitAbsensi!='') {
    		$.ajax({
    			url: apiPath + `&act=update`,
    			type: 'POST',
    			dataType: 'json',
    			data: $SubmitAbsensi+"class_id="+$ClassId+"&attendance_day="+$Tanggal+"&period_id=<?= (int)$_GET['id'] ?>",
    		})
    		.done(function() {
    			setTimeout(
    				function(){
    					Swal.fire("Sukses!", "Data Absensi Berhasil Dikirim", "success");
    				}, 500);
    		});
    	}
    });

    // Aksi Hide Form Edit
    $(document).on('click','.removeForm',function(){
    	$('#edit-tab').hide();
    	$('a[href="#data"]').trigger('click');
    	$('.form').trigger("reset");
    	$('.form').trigger("change");
    	setTimeout(function(){ 
    		OnPageEdit = false;
    		table.ajax.url(apiPath + "&act=datatables").load(null, true);
    	}, 200);
    });

    // Aksi Hapus
    $(document).on('click','[data-del]',function(){
    	var $param = $(this);
    	var deleteId = $param.data('del');

    	Swal.fire({
    		title: "Apakah Anda Yakin Menghapus Ingin Data?",
    		text: "Jika Yakin, Silahkan Pilih Ya!",
    		type: 'warning',
    		showCancelButton: true,
    		confirmButtonText: 'Ya, Hapus Sekarang',
    		cancelButtonText: 'Tidak, Batalkan!',
    		confirmButtonColor: '#d33',
    		cancelButtonColor: '#333',
    	}).then(isConfirm =>
    	{
    		if (isConfirm.value==true) {                       
    			$.ajax({
    				type: 'GET',    
    				url: apiPath + "&act=delete",
    				data: `${idKey}=`+deleteId,
    				success: function(msg){
    					Swal.fire("Terhapus!", msg, "success");
    					getData();
    				},
    				error: function (request, kategori_sekolah, error) { 
    					Swal.fire("Terjadi Kesalahan", request.responseText, "error");
    				}
    			});
    		} else {
    			Swal.fire("Dibatalkan", "Berhasil Membatalkan Penghapusan", "error");
    		}
    	})
    });


    function radioAbsensi(data, value, keterangan){
    	return `<label><input type="radio" class="radioAbsensiNya StatusAbsensiNya status_${value} absensi_${data} absensi${value}_${data}" name="status[${data}]" value="${value}" disabled> ${value} </label>`;
    }

    $("#allHadir").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_H").prop("checked", true);
    	$("#allHadir").prop("checked", true);
    });

    $("#allAlpha").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_A").prop("checked", true);
    	$("#allAlpha").prop("checked", true);
    });

    $("#allMinggat").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_M").prop("checked", true);
    	$("#allMinggat").prop("checked", true);
    });

    $("#allIzin").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_I").prop("checked", true);
    	$("#allIzin").prop("checked", true);
    });

    $("#allSakit").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_S").prop("checked", true);
    	$("#allSakit").prop("checked", true);
    });

    $("#allTelat").click(function(){
    	$(".radioAbsensiNya").prop("checked", false);
    	$(".status_T").prop("checked", true);
    	$("#allTelat").prop("checked", true);
    });
  }
</script>