<?php
error_reporting(0);

require(__DIR__ . '/modules/config.php'); 

// get parameter halaman dinamis
$page = @$_GET['p'];

if (@$_SESSION['IsLogin']!=true) {
	echo "<script>location.href='index.php'</script>";
	exit();
} else if (empty($page)) {
	echo json_encode(['error'=>true,'status'=>'404 Not Found']);
	exit();
}

// halaman dinamis berdasarkan parameter q
if (!(in_array($page, $allPages['api']))) {
	// jika tidak ada didalam list semua_halaman
	echo json_encode(['error'=>true,'status'=>'404 Not Found']);
} else if (in_array($page, $acl['api'][@$_SESSION['Level']]) && preg_match("/^[a-zA-Z\/_-]*$/",$page) ) {
	// Memanggil Library Server Side Rendering DataTables
	require(__DIR__ . "/modules/ssp.class.php");
	require(__DIR__ . "/modules/_api/".$page.".php");
} else {
	// jika tidak memiliki akses
	echo json_encode(['error'=>true,'status'=>'403 Forbidden']);
}